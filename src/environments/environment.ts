// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  apiUrl: 'https://dineroconopciones.herokuapp.com/api',
  urlRedirect: 'https://discordapp.com/api/oauth2/authorize?client_id=445639978282909699&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2Fauth&response_type=code&scope=identify%20guilds%20email%20guilds.join%20connections%20gdm.join'
};
