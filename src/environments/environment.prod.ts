export const environment = {
  production: true,
  apiUrl: 'https://dcoweb.app/api/',
  urlRedirect: 'https://discordapp.com/api/oauth2/authorize?client_id=445639978282909699&redirect_uri=https%3A%2F%2Fdcoweb.app%2Fauth&response_type=code&scope=identify%20guilds%20guilds.join%20email%20connections%20gdm.join'
};
