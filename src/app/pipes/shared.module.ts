import { NgModule } from '@angular/core';
import { SafePipe } from './youtube.pipe';

@NgModule({
  declarations:[SafePipe],
  exports: [SafePipe]
})
export class SharedModule {}
