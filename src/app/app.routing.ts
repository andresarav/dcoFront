import { RouterModule, Routes } from '@angular/router';
import { FeedComponents } from './feed/feed-component';
// import { DetailsComponent } from './detalle-proyecto/detalle-proyecto.component';
// import { ModalComponent } from './emergente/popUp/modal.component';
import { AuthComponent } from './auth/auth-component';
import { PROJECT_ROUTES } from './proyecto/detalle-proyecto/proyecto.routing';
import { ADMIN_ROUTES } from './admin/admin.routing';

const APP_ROUTES: Routes = [
  { path: '', component: FeedComponents, pathMatch: 'full'},
  { path: 'auth', component: AuthComponent },
  { path: 'projects', children: PROJECT_ROUTES },
  { path: 'admin', children: ADMIN_ROUTES },
];

export const Routing = RouterModule.forRoot(APP_ROUTES);
