import { DetailsComponent } from './detalle-proyecto.component';
import { FeedComponents } from '../../feed/feed-component';

export const PROJECT_ROUTES = [
  { path: '', component: FeedComponents },
  { path: ':id', component: DetailsComponent }
];
