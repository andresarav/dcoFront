import { Component, ViewChild, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { Project } from './proyecto.model';
import { NgForm } from '@angular/forms';
import { Update,
         Coin,
         Pool,
         Exchange,
        Social,
        Multimedia,
        User,
        Membership,
        Wallet,
        Payments,
        CMC,
        coinCMC
      } from '../../models/modelos.model';

import { Steemit } from '../../panelFix/steemit.model';
import {FeedService} from '../../feed/feed.service';
import {AuthService} from '../../auth/auth.service';
import {GlobalService} from '../../global.service';

import { ActivatedRoute, Router } from '@angular/router';



declare var jQuery:any;
declare var $:any;

// import { MatTableDataSource, MatSort } from '@angular/material';


 @Component({
   selector: "app-detail2",
   templateUrl:"./detalle-proyecto.component.html",
   styleUrls:["./detalle-proyecto.component.css"],
 	 providers: [FeedService]
 })

export class DetailsComponent implements OnInit {
  constructor(private feedService: FeedService,
  public snackBar: MatSnackBar,
  private route: ActivatedRoute,
  private globalService: GlobalService,
  private authService: AuthService,
  private router: Router
){}

// modal:any = [
//   {title:'Titulo', description:'descripcion'},
//   {title:'Titulo', description:'descripcion'}
// ];

liked:any = '';
disLiked:any = '';
likes:boolean;
disLikes:boolean;
barLikes:number;
barLikesProgress:number;
x:any;

status:string ='actives';
mostrar:string = '';

diasTotal:number;
diasTranscu:number;
hoy:Date = new Date;
barDiasProgress:number;
dataCMC:CMC = new CMC();
priceUsd:number = 0;

suscrito:boolean;

sub:any;

// project:Project;

toggleModalHover(mensaje){

// alert(mensaje);
this.mostrar = mensaje;

  // if(this.status === 'active'){
  //   return  this.status = 'INactive';
  // }
  return this.status = 'active';
}

cerrar(){
  return  this.status = 'INactive';
}

Msg:any = [
  {title:'ADVERTENCIA',
description:'Esta información es confidencial de cada uno de los proyectos ICO, es ilegal mencionar los Exchange donde harán lanzamiento, esto puede conllevar a un cierre del proyecto por parte de la SEC (Securities and Exchange Commission), un proyecto sólido jamás dirá el Exchange en el que se comercializará, por lo menos en fase ICO..',
clase:'Exch',
},

  {title:'ADVERTENCIA',
  description:'1. No hablamos sobre nuestras piscinas con nadie que no esté relacionado con este grupo.',
  regla2:'No nos contacte con ofertas que haya adquirido. Solo Francis @TheKinGeek (El Francis)  y @Mr.Killer  contacta directamente con los equipos de ICO, Si nos enteramos que están hablando en nombre de DCO sin previa autorización, al menos que el los haya autorizado.',
  regla3:'3. NO PUBLIQUE LA DIRECCIÓN DE LA PISCINA EN NINGUN CANAL FUERA DE NUESTRO CANAL, NO HAGA PERDEMOS UN NEGOCIO POR UNA FUGA DE INFO PRIVADO, TODOS LOS NEGOCIOS DE PISCINAS SON PRIVADA CON LA EMPRESA Y SE FIRMA UN CONTRATO DE NO DIVULGACIÓN, SI LO ROMPEMOS PERDEMOS EL NEGOCIO Y SI ENCONTRAMOS QUIEN LO HIZO SERA BANEADO DE POR VIDA.',
  clase:'Warning',
  }
];


modals:any = [
  {title:'ADVERTENCIA', description:'descripcion'},
  {title:'modal2', description:'descripcion'},
];

data='22';

//Ejemplo Modelo Proyecto---------------------------------------------------------------


coinData: Coin = new Coin(
  'BOON TECH',
  'BNTCH',
  '0,04', //PrecioUsd
  '0000001234',//PrecioEth
  9000, //MounthPerEth
  '10.000.000',  //Hard Cap USD
  '80.000.000',   //Total Supply
  '50.000.000',   //Token Bloq
  'Estos tokens estaran bloqueados por x cantidad de tiempo'    //Token Bloq
);

pool: Pool = new Pool(
  'mensaje de alerta',
  1,
  4,
  'https://www.primablock.com',
  new Date(2018,2,17),
  true,
  false
);


multimedia:Multimedia[] =[
  new Multimedia ('http://welltechnically.com/wp-content/uploads/2013/09/android-widescreen-wallpaper-14165-hd-wallpapers-700x300.jpg','imagen'),
  new Multimedia ('http://youghaltennisclub.ie/wp-content/uploads/2014/06/Tennis-Wallpaper-High-Definition-700x300.jpg','imagen'),
  new Multimedia ('http://youghaltennisclub.ie/wp-content/uploads/2014/06/Tennis-Wallpaper-High-Definition-700x300.jpg','imagen'),
  new Multimedia ('http://youghaltennisclub.ie/wp-content/uploads/2014/06/Tennis-Wallpaper-High-Definition-700x300.jpg','imagen'),
];

socialNet:Social[] =[
  new Social ('bitcoin', 'https://www.bitcointalk.com'),
  new Social ('reddit', 'https://www.reddit.com'),
];



exc:Exchange[] =[
  // new Exchange ('Binance', 'link'),
  // new Exchange ('EtherDelta', 'link'),
];

news:Update[] =[
  new Update ('Titulo Noticia Genesis',
  'Descripción Noticia Genesis',
  'www.linkNoticia.com',
  new Date,
  new Date,
 )
];

// project: Project = new Project(
//   2231,
//   'Titulo',
//   'ico',
//   'Vip',
//   new Date,
//   this.pool,
//   this.coinData,
//   this.reaction,
//   this.exc,
//   this.news,
//   this.subscriptor,
//   this.socialNet,
//   this.multimedia,
//
//   '30%',
//   '10',
//   '1',
//   'www.websiteProyecto.com',
//   'www.LinkWhitePaper.com',
//   new Date(2018,0,19),
//   new Date(2018,3,25),
//   'Descripción Proyecto',
//   'Equipo ',
//   'Estado del Producto',
//   'Socios',
//   'Dirección Contrato Ether'
// );



//Ejemplo Modelo usuario---------------------------------------------------------------






subscrip:number[] =[
  34423,
  34423,
];

likeProjects: number[] = [
  23312,
  55323,
];

dislikeProjects: number[] = [
  21377,
];

membership: Membership = new Membership(
  new Date,
  new Date
);

wallets: Wallet[] = [
  new Wallet('bitcoin', '3gfdsf743uwf7w7934qef6'),
  new Wallet('ethereum', 'df5sdfs568dfs68dfsd68fs'),
];

payments: Payments[] = [
  new Payments(new Date(2018,1,10), '3gfdsf743uwf7w7934qef6'),
  new Payments(new Date(2018,2,10), 'df5sdfs568dfs68dfsd68fs')
];



// usuario: User = new User(
//   2231254,
//   null,
//   'Andreas Araveug',
//   'Andresaraveug',
//   this.wallets,
//   this.payments,
//   'andreasarav@gmail.com',
//   'asdd1234d',
//   'vip',
//   this.subscrip,
//   this.likeProjects,
//   this.dislikeProjects,
//   true,
//   this.membership
// );







faqP:string = 'pool';



// project2: Project2 = new Project2(
//     '3556',
//     'BoonTech',
//     new Date,
//     'BNC',
//     '10 millones',
//     '100 millones',
//     '05 millones',
//     '30%',
//     '50 Eth',
//     '0.2 Eth',
//     'http://www.facebook.com',
//     'whitepaper',
//     'exchange',
//     '100',
//     '10',
//     '10',
//     '10',
//     '10',
//     new Date,
//     new Date,
//     true,
//     `ICO de Data tipo DataWallet, Datum, esta tiene la diferencia que es la primera ICO sobre la Blockchain de EOS, y parte de YC, - La aplicación EOS sería favorable para la comunidad EOS; EOS actualmente en el límite del mercado n.°10
// - Los corredores de datos ganan mucho dinero. En 2012, se informó que Acxiom había ganado $ 1,13 mil millones, ganando una ganancia de $ 77.26 millones.
// - Los piratas informáticos violaron los sistemas de Equifax y robaron datos de identificación personal de más de 140 millones de estadounidenses.
// `,
//     `- Antecedentes del equipo de YC Alumni; YC lote 2015;
// - Asesor Dino Amaral - Ph.D. Criptografía 2008-2013 en Brasil
// - Jason Hamlin, 4 años en AC Nielson como gerente sénior (una compañía global de información, datos y medición con ingresos de $ 6.2 mil millones en 2015)
// `,
//     `- Carpeta INSTAR construida para pruebas beta
// - La implementación beta está en curso con Gambeal (aplicación del cofundador)
// `,
//     `1. En conversaciones con empresas de seguro, compañías de nómina
// 2. Asociaciones confirmadas con Quanstamp, Partisia, Mobius ($ 80M cap)
//     - Proporcionar auditoría QSP para el protocolo de investigación de mercado de EOS Blockchain
//     - Asociación con Partisia en Computación segura multipartita. El intercambio seguro de Partisia fue la primera aplicación a gran escala y el uso comercial de SMC en el mundo. https://partisia.com/mpc-goes-live/; Tecnología patentada
// `,'0x72c9f1BBCba8a4a0d6460ff1828d0abd2cF455'
// );

// projects:Project[] = new Array(10).fill(this.project); //users es un array del modelo User

// cantidadTokens:string = this.project.mountPerEther;

advertenciaPool:string = 'NO PUBLIQUE LA DIRECCIÓN DE LA PISCINA EN NINGUN CANAL FUERA DE NUESTRO CANAL, NO HAGA PERDEMOS UN NEGOCIO POR UNA FUGA DE INFO PRIVADO, TODOS LOS NEGOCIOS DE PISCINAS SON PRIVADA CON LA EMPRESA Y SE FIRMA UN CONTRATO DE NO DIVULGACIÓN, SI LO ROMPEMOS PERDEMOS EL NEGOCIO Y SI ENCONTRAMOS QUIEN LO HIZO SERA BANEADO DE POR VIDA';
index:any;

likeProject(){//validamos si el usuario ya ha hecho like a este proyecto...
  return  this.liked = this.project.userPreferences.like;
}
disLikeProject(){//validamos si el usuario ya ha hecho disLike a este proyecto...
return  this.disLiked = this.project.userPreferences.dislike;
}

quitarLike(){
  // this.index = this.usuario.likeProjects.indexOf(this.project.id);
  // this.usuario.likeProjects.splice(this.index,1)
  this.project.likes-=1;
  this.upLikeBar(this.project.likes, this.project.dislikes);
  console.log(`este es el % de la barra like: ${this.barLikesProgress}`);
  return this.likes=false;
}

sumarLike(){
  this.project.likes+=1;
  this.upLikeBar(this.project.likes, this.project.dislikes);
  console.log(`este es el % de la barra like: ${this.barLikesProgress}`);

  return this.likes=true;
};


sumarDisLike(){
  this.project.dislikes+=1;
  // this.usuario.disLikeProjects.push(this.project.id);
  this.upLikeBar(this.project.likes, this.project.dislikes);
  console.log(`este es el % de la barra like: ${this.barLikesProgress}`);
  return this.disLikes=true;
};


quitardisLike(){
  // this.index = this.usuario.disLikeProjects.indexOf(this.project.id);
  // this.usuario.disLikeProjects.splice(this.index,1)
  this.project.dislikes-=1;
  this.upLikeBar(this.project.likes, this.project.dislikes);
  return this.disLikes=false;
};

upLikeBar(likes, dislikes){
  console.log(`likes: ${likes} - dislikes: ${dislikes}`);
  //sacamos porcentaje equivalente al fill de la barra de likes
  this.barLikes = likes + dislikes;
  //redondear a entero el resultado con la función Math.round
  this.barLikesProgress = Math.round((likes*100)/this.barLikes);

  if(isNaN(this.barLikesProgress)){
    return this.barLikesProgress = 0;
  }
  return this.barLikesProgress;
  // console.log(`este es el fill de la barra: ${this.barLikesProgress}`);
};

// dateProgressBar(){
//   // this.hoy = new Date;
//   let today = this.hoy.getTime();
//   let inicia = this.project.startDate.getTime();
//   let termina = this.project.endDate.getTime();
//
//
//   var diff = termina - inicia;
//   var diff2 = today - inicia;
//
//   this.diasTotal = Math.round(diff/(1000*60*60*24));
//   // console.log(this.diasTotal);
//   this.diasTranscu = Math.round(diff2/(1000*60*60*24));
//
//
//
//   this.barDiasProgress =  Math.round((this.diasTranscu*100)/this.diasTotal);
//   $('.progresado').css('width',`${this.barDiasProgress}%`);
//
//   // return console.log(this.barDiasProgress);
//
//
//   // return console.log(`Inicia: ${inicia} y termina ${termina}`);
// };


openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

deleteLikeDis(){
  return this.feedService.deleteReaction(this.project.id)
      .subscribe(
      () => {console.log('eliminación realizada con exito')},
      error => this.feedService.handleError(error)
    );
}

like(){

  if(!this.authService.isLoggedIn()){
    this.router.navigate(['/auth']);
    return this.accesDenied();
  }

if(!this.likes){//si el proyecto NO tiene like, sumele 1, de lo contrario quiteselo:
    if(this.disLikes){
    this.quitardisLike();
    this.deleteLikeDis();
  }
  this.sumarLike();
  return this.feedService.likeProject(this.project.id)
      .subscribe(
      () => this.openSnackBar('¡Me gusta este proyecto!', ''),
      error => this.feedService.handleError(error)
    );
}

 this.quitarLike();
return this.deleteLikeDis();

}



disLike(){
  if(!this.authService.isLoggedIn()){
    this.router.navigate(['/auth']);
    return this.accesDenied();
  }

  if(!this.disLikes){//si el proyecto NO tiene disLike, entonces SUMELE 1 Dislike
    if(this.likes){
      this.quitarLike();
      this.deleteLikeDis();
    }
   this.sumarDisLike();
  return this.feedService.dislikeProject(this.project.id)
      .subscribe(
      () => {console.log('operacion realizada con exito')},
      error => this.feedService.handleError(error)
    );
  }// si el proyecto si tiene disLike, entonces restele 1 like
this.quitardisLike();
return this.deleteLikeDis();
}


subscriProject(){
  return this.feedService.suscribir(this.project.id)
      .subscribe(
      () => {console.log('subscription realizada con exito')},
      error => this.feedService.handleError(error)
    );
}

unSubscriProject(){
  return this.feedService.deSuscribir(this.project.id)
      .subscribe(
      () => {console.log('DESUBSCRIPTION realizada con exito')},
      error => this.feedService.handleError(error)
    );
}

accesDenied(){
  return this.globalService.openSnackBar('No tiene permisos para llevar a cabo esta acción', 'ACCESO DENEGADO');
}

subscribir(){
  // console.log(this.usuario);
console.log(this.project.userPreferences.subscribed);

if(!this.authService.isLoggedIn()){
  this.router.navigate(['/auth']);
  return this.accesDenied();
}



if(!this.suscrito){//si no estas subscrito al proyecto, entonces lo suscribe
  // if(!this.project.userPreferences.subscribed){//si no estas subscrito al proyecto, entonces lo suscribe
    this.suscrito = true;
    // console.log(this.suscrito);
    this.openSnackBar('¡En hora buena, recibiras actualizaciones vía e-mail!','Suscrito');
    return this.subscriProject();
  }

  // console.log(this.suscrito);
  this.suscrito = false;
  return this.unSubscriProject();
}

// VARIABLES TYPESCRIPT DEL CRONOMETRO CUENTA REGRESIVA-----------------------------------------------------------------------------------------------------

dias:number;
horas:number;
minutos:number;
segundos:number;
estadoPool:boolean;

countDown(endDate:Date){

  var countDownDate = new Date(endDate.getFullYear(),endDate.getMonth(),endDate.getDate()).getTime();
  var now = new Date().getTime();

  var x = setInterval(()=> {


      var distance = countDownDate - now;

      this.dias = Math.floor(distance / (1000 * 60 * 60 * 24));
      this.horas = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      this.minutos = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      this.segundos = Math.floor((distance % (1000 * 60)) / 1000);
      this.estadoPool = true;


      if (distance < 0) {
          clearInterval(x);
          this.estadoPool = false;
          // document.getElementById("demo").innerHTML = "FINALIZADO";
      }
  }, 1000);

}
// ---------------------------------------------------------------------------------------------------------------------------------------------------

  // project:Project;
  private project;


cuentaPool:any;
loading:boolean = true;
endPool:boolean;
estadoControlMenu:any;
alternoResponsive:string;
// likes:boolean;
// disLikes:boolean;
// barLikes:number;
// barLikesProgress:number;


role:any;

volumen24:any;
marketCap:any;
circulatinS:any;
hardCap:any;
totalSupply:any;
tokenBloq:any;


cargarRenderIMG(){
    var x = setTimeout(()=> {
      $("#descProject img").css("width","100%");
    },500)
}

  ngOnInit(){

    var x = setTimeout(()=> {
      $(".itemUser2 img").css("width","100%");
    },2000)

    console.log('CMC VIRGENCITO: ', this.dataCMC);
    if(this.authService.isLoggedIn()){
      this.role = this.authService.getRoleStorage();
    }else{
      this.role = "user";
    }

    this.feedService.FuncionScrollDetail(true);

//falta hacer un condicional que detecte si el usuario es administrador y posteriormente modifique estilos para acomplar la barra de admin
this.globalService.emitChange('a la ver');
this.globalService.sendButtonSave(false);

// this.globalService.noticeEmitted.subscribe(respuesta => {
//   this.project.news.push(respuesta);
// });


    this.sub = this.route.params.subscribe(params => {
      //esta es la variable de ruta en ../question.routing (:id)

      // console.log(`${params.id} este es el ID`);
      this.feedService
          .getProject(params.id)
          .then((projecto:Project)=>{
            this.project = projecto;
            console.log('CONSULTA PROYECTOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO');
            console.log(this.project);
            this.loading = false;
            //falta hacer condicional, la siguiente linea de codigo solo se ejecuta para el usuario administrador
            // this.globalService.emitChange(this.project.id);
            this.globalService.sendProject(this.project);


            if(this.project.pool){
              const endDate = new Date(this.project.pool.endDate);
              const dateNow = new Date();
              if(dateNow>endDate){
                this.endPool = true;
              };
              this.countDown(endDate);
            };

            this.likes =  this.likeProject();
            this.disLikes =  this.disLikeProject();
            console.log(`este es el like: ${this.likes} y estos los dislikes ${this.disLikes}`)

            this.upLikeBar(this.project.likes, this.project.dislikes);
            this.suscrito = this.project.userPreferences.subscribed;
            this.cargarRenderIMG(); //para aplicar width del 100% a las imagenes de descripción, se hace con jquery parcialmente

            // aqui consulta aapi cmc
            if(this.project.type == 'market'){
            this.feedService
                .getProjectDetailCmc(this.project.coinmarketcapId)
                .then((response) => {

                  this.dataCMC = response.data;

                  // var cadena = dataCMC.quotes.USD.price;
                  // this.priceUsd;
                  // cadena.substr(1,3);

                  this.volumen24 = this.feedService.number_format(String(this.dataCMC.quotes.USD.volume_24h));
                  this.marketCap = this.feedService.number_format(String(this.dataCMC.quotes.USD.market_cap));
                  this.circulatinS = this.feedService.number_format(String(this.dataCMC.circulating_supply));
                  // this.marketCap =
                  console.log('Formato Numero', this.volumen24);
                  // console.log('Respuesta CMC:', this.dataCMC, this.dataCMC.quotes.ETH.volume_24h.toFixed(0));
                })
            }else{
                this.hardCap = this.feedService.number_format(String(this.project.hardCap));
                this.totalSupply = this.feedService.number_format(String(this.project.totalSupply));
                this.tokenBloq = this.feedService.number_format(String(this.project.tokenBloq));
            }

          })
          .catch(()=>{
            this.router.navigate(['/auth'])
          })


    });


    this.globalService.changeEmitted.subscribe(
      response => {
        console.log(response);
        this.estadoControlMenu = response;
        console.log('eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeste es el estado del componente-------------------');
        console.log(this.estadoControlMenu);
      }
    );





    this.globalService.alternoEmitted.subscribe(res => {
      switch (res) {
        case 'abrir':
          this.alternoResponsive = 'abrir';
          break;
          case 'cerrar':
            this.alternoResponsive = 'cerrar';
            break;
      }
    });



    if(this.authService.isLoggedIn() && this.role == 'admin'){
      // return this.router.navigate(['/auth']);
      return  this.globalService.panelAdminVisible('visible', 'detalle');
    }else{
      return this.globalService.sendLoadings(false);
    }



  }


}
