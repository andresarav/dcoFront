import { Update, Pool, Social, Coin, Exchange,
Multimedia, UserPreferences } from "../../models/modelos.model";

export class Project{

	constructor(
		public id?:any,
			public title?: string,
			public stars?: number,
			public type?: string,
			public target?: string,
		public createdAt?: Date,
		public pool?: Pool,
			public exchanges?:Exchange[],
		public news?:Update[],
		public subscriptions?:number,
			public socialNetworks?:Social[],
			public multimedia?:Multimedia[],
		public likes?: number,
		public dislikes?: number,
		public bono?: number,
		public maxEth?: number,
		public minEth?: number,
    public webSite?: string,
    public whitePaper?: string,
		public startDate?: Date,
		public endDate?: Date,
		public description?: string,
		public team?: string,
		public product?: string,
		public partner?: string,
		public contractAdress?: string,
		public totalDays?: number,
		public passedDays?: number,
		public percentagePassed?: number,
		public userPreferences?: UserPreferences,
		public coinId?: string,
		public poolId?: string,
		public coinName?: string,
		public simbol?: string,
		public usdPrice?: number,
		public ethPrice?: number,
		public amountPerEth?: number,
		public hardCap?: number,
		public totalSupply?: number,
		public tokenBloq?: number,
		public bloqMessage?: string,
		public hasPool?: boolean,
		public coinmarketcapId?: number,
		public image ?: any,
	){}
}


export class proyectico{

	constructor(
			public id?:any,
			public title?: string,
			public stars?: number,
			public target?: string,
			public createdAt?: Date,
			public socialNetwork?:Social[],
			public multimedia?:Multimedia[],
	){}

}
