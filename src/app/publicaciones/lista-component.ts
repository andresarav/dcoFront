import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import  {FeedService}  from '../feed/feed.service';
import { ProjectList, CoinData, Ad, ModalResponse } from './lista.model';
import { Multimedia } from '../models/modelos.model';
import {AuthService} from '../auth/auth.service';
import {GlobalService} from '../global.service';

declare var jQuery:any;
declare var $:any;


 @Component({
   selector: "app-lista-publi",
   templateUrl:"./lista.component.html",
   styleUrls:["./lista.component.css"],
 })

export class ListComponent implements OnInit {

  constructor (
    public feedService:FeedService,
    public authService:AuthService,
    public globalService:GlobalService,
  ) {}


  // @Input() alimentadorFeed:any[];

loading:boolean = true;
status:string;
mostrar:string;
alimentadorFeed:any[];

@Output() sendMoreProject = new EventEmitter();


onNotifyClicked(message:string):void{
this.status = message;

    console.log(this.status);
}

getBarProgress(payload){
  return `${payload}%`;
}

toggleModalHover(mensaje){
this.mostrar = mensaje;
// if(this.status != 'active'){
  console.log(mensaje);
return  this.status = 'active';
// }
// return  this.status = 'inactive';

}


url:any;


consulta(payload){
  console.log(payload);
};


role:any;

  ngOnInit(){


    if(this.authService.isLoggedIn()){
      this.role = this.authService.getRoleStorage();

    }else{
      this.role = "user";
    }




    this.feedService
    .getProjectList(0)
    .then((projects:any[])=> {
      // this.projects = projects;
      // this.loading = false;
      console.log('getPROJECTLISSSSST HERE:...', projects)
      this.sendMoreProject.emit(true);
      this.loading = false;
      return this.alimentadorFeed = projects;
    });


    this.globalService.listProjectEmitted.subscribe( (data:any) => {
    console.log(data.visual);
      if(data.visual == 'all'){
        data.projects.map(project => {
          this.alimentadorFeed.push(project);
        })
        console.log(this.alimentadorFeed);
      }
    })


  }

}
