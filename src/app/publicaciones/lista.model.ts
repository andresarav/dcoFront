import { Update } from "../models/modelos.model";
import { Multimedia } from '../models/modelos.model';

// Modelo Lista


export class ProjectList{

	constructor(
		public id:any,
		public comment: string,
		public bono?: number,
		public stars?: number,
    public type?: string,
		public releaseType?: string,
		public target?: string,
		public createdAt?: Date,
		public hasPool?: boolean,
		public coin?:CoinData,
    public multimedia?:Multimedia[],
		public startDate?: Date,
		public endDate?: Date,
		public totalDays?: number,
		public passedDays?: number,
		public percentagePassed?: number
	){}
}

export class CoinData{
	constructor(
			public name?: string,
			public simbol?: string,
			public usdPrice?: string,
    	public hardCap?: string,
			// public bono?: number,
    public minEth?: number
		){}
  }

  export class ModalResponse{
  	constructor(
  		public poolDate?: Date,
      public news?: Update[]
  		){}
    }

//Modelo anuncio

  export class Ad{
    constructor(
      public title?: string,
			public target?: string,
	    public description?: string,
			public type?: string,
			public links?: any,
			// public links?: Multimedia[],
			// public createdAt?: Date,
			// public releaseType?: string,
      ){}
    }
