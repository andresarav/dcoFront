import { Project } from '../proyecto/detalle-proyecto/proyecto.model';



// Modelo CoinmarketCap
export class CMC{
	constructor(
    	public id?: number,
			public circulating_supply?: number,
			public max_supply?: string,
			public name?: string,
			public quotes?: {ETH:coinCMC, USD:coinCMC},
			public symbol?: string,
			public total_supply?: string,
		){}
}

// documento2 = {
// 	"id": "23452",
// 	"circulating_supply": "10000",
// 	"max_supply": "40000",
// 	"name": "Ethereum",
// 	"symbol": "ETH",
// 	"quotes": {"ETH":"UNO", "USD":"MIL"},
// 	"total_supply": "30000",
// }

//Coin model CMC
export class coinCMC{
	constructor(
		public market_cap?: number,
		public percent_change_1h?: number,
		public percent_change_7d?: number,
		public percent_change_24h?: number,
		public price?: number,
		public volume_24h?: number,
	){}
}



// Modelo noticia
export class Update{
	constructor(
    		public title?: string,
				public description?: string,
				public link?: string,
			public date?: Date,
			public createdAt?: Date,
			public projectId?: string,
			public id?: string
		){}
}


//Modelo Piscina
export class Pool{
	constructor(
			public warning?: string,
    	public minEth?: number,
			public maxEth?: number,
			public link?: string,
			public endDate?: Date,
		public active?: boolean,
		public reclaim?: boolean,
		public bono?: number,
		public createdAt?: Date,
		){}
}


//Modelo reactions
// export class Social{
// 	constructor(
//     public likes?: number,
// 		public dislikes?: number
// 		){}
// }

//Modelo Info Moneda
export class Coin{
	constructor(
			public coinName?: string,
			public simbol?: string,
			public usdPrice?: string,
			public ethPrice?: string,
			public amountPerEth?: number,
			public hardCap?: string,
			public totalSupply?: string,
			public tokenBloq?: string,
    	public bloqMessage?: string,
			public id?: string
		){}
}


//Modelo redes sociales
export class Social{
	constructor(
    public name?: string,
		public link?: string
		){}
}

//Modelo contenido multimedia (videos youtube proyecto)
export class Multimedia{
	constructor(
    public link?: string,
		public name?: string
		){}
}

//Modelo contenido  reaction, susbscriptions
export class UserPreferences{
	constructor(
    public like?: boolean,
		public dislike?: boolean,
		public subscribed?: boolean
		){}
}


//Modelo Exchanges
export class Exchange{
	constructor(
    public name?: string,
		public link?: string,
		public pendiente?: boolean,
		){}
}



//Modelos a referencia clase User-------------------------------------------------------------------------


//Modelo user
export class User{
	constructor(
		public id: number,
			public birthday: Date,
			public name?: string,
				public userName?: string, //solo el administrador puede cambiar este campo
		public wallets?: Wallet[],
		public payments?: Payments[],
				public email?: string, // solo el administrador puede cambiar el email de un usuario
				public role?: any, //solo el administrador puede cambiar este campo
		public subscriptions?: Subscription[],
		public blocked?: boolean, //condicional para habilitar o inhabilitar la visualización de la información de un usuario dentro de la plataforma
    public txAudit?: boolean, //cada vez que el usuario paga su membresía, esta propiedad pasara a false, con esto el usuario entrara en un estado pendiente de revisión de su txid manual por un administrador, para verificar que se hizo efectivo el pago
				public membership?: Membership,
		public password?: string,
			public perfilPic?: string,
			public createdAt?: Date,
		public level?: number,
		public passedDays?: number,
		public percentagePassed?: number,
		public totalDays?: number,
		public verifyMembership?: any,
		public lastName?: string,

		){}
}

export class Membership{
	constructor(
		public startDate: Date,
		public endDate: Date,
		){}
}

export class Subscription{
	constructor(
		public coinName?: string,
		public pool?: Pool, // del modelo pool solo necesitamos traer la propiedad reclaim y link, para avisar a nuestro usuario que ya se puede reclamar los token de una piscina
		public multimedia?: Multimedia,
		public userPreferences?: any[],
		public createdAt?: Date,
		public projectId?: any
		){}
}

// export class Wallet{
// 	constructor(
// 		public red: string,
// 		public address: string,
// 		public walletName?: string,
// 		public userId?: string,
// 		){}
// }

export class Wallet{
	constructor(
		public type: any,
		public address: string,
		public name?: string,
		public userId?: any,
		public id?: string,
		){}
}

export class Payments{
	constructor(
		public createdAt?: Date,
		public txId?: string,
		public wallet?: Wallet,
		public verified?: boolean,
		public type?: string,
		public walletId?: any,
		public userId?: any,
		public id?: string,
		){}
}
