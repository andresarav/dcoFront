import { Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
// import { ContenidoModal } from './contenido.model';
import { Multimedia, User, Payments } from '../../models/modelos.model';


import {GlobalService} from '../../global.service';
import {FeedService} from '../../feed/feed.service';
import {AuthService} from '../../auth/auth.service';

declare var jQuery:any;
declare var $:any;

 @Component({
   selector: "app-modal-update",
   templateUrl:"./modalProfile.component.html",
   styleUrls:["./modalProfile.component.css"],
 })

export class ModalProfileComponent implements OnInit {

constructor(
  private globalService:GlobalService,
  private feedService:FeedService,
  private authService:AuthService
){}

statusModals:string = "inc";
user:User;
payments:Payments[] = [];
open:number;

caducaM:number = 0;
anchito:number;
carinito:string;

visualizar:string;
traction:number = 0;

loading:boolean = true;
statusModalsProfile:string = "";
userName:string = "Nombre discord";
perfilPic:string;
makePayment:boolean;
userStorage:User;

ngOnInit(){

  this.userStorage = this.authService.getUserStorage();

  this.caducaM = this.userStorage.totalDays - this.userStorage.passedDays;
  this.anchito = this.userStorage.percentagePassed;
  this.carinito = this.feedService.progresadoBar(this.anchito);
  // console.log(this.user.passedDays);
  this.userName = this.userStorage.userName;
  this.perfilPic = this.userStorage.perfilPic;
  this.globalService.updateMembershipEmitted.subscribe((res:any) => {
    // console.log(res);
     this.statusModals = res.status;
     this.loading = true;
     this.makePayment = res.payment;



    this.feedService.getUser()
        .then((res:User) => {
          this.user = res;


               if(this.makePayment == true){
                   var y = setTimeout(()=> {this.visualizar = "historial"}, 400)
                   this.loading = false;
                   this.statusModalsProfile='active';
                   this.traction = 50;
                 return this.open = 0;
               }else if(this.makePayment == false){
                 this.loading = false
                 this.statusModalsProfile='active';
                 this.visualizar = "general";
                 this.traction = 0;
                 return this.open = -100;
               }

        },
        error => console.log(error)
      )

  })




    // this.user = JSON.parse(localStorage.getItem('user'));
    // this.anchito = 50;


    // if(this.user != null){
    //   this.payments = this.user.payments;
    // }

    // console.log('sssssssssssaaaaaaaaaaaaaaaaaaaaaaaassssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss');
    // console.log(this.anchito);






}




      ocultar(){
        this.globalService.sendUpdateMembership('cerrar', false);
      };

}
