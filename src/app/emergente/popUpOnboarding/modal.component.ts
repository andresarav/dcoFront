import { Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import { ContenidoModal } from './contenido.model';
import { Multimedia } from '../../models/modelos.model';

declare var jQuery:any;
declare var $:any;

 @Component({
   selector: "app-modal",
   templateUrl:"./modal.component.html",
   styleUrls:["./modal.component.css"],
 })

export class ModalComponent implements OnInit {

@Input() statusModal:string;
@Input() contenidoModal:string;
@Output() statusModalReturn: EventEmitter <string> = new EventEmitter<string>();
@Output() contenidoModalReturn: EventEmitter <string> = new EventEmitter<string>();

status:string = "hola";
controlPosition:any = 'inicio';
feedTemplate:any[];
paso:number = 0;
traction:number = -20;
url:any = '../../../assets/onboarding/newUser/';
// @Input() Datas:any;
// status:string = 'actives';

 links:Multimedia[] = [
   new Multimedia('titulo1', 'link'),
   new Multimedia('titulo2', 'link2'),
 ]


newUser:ContenidoModal[] = [
  new ContenidoModal(
    0,
    'newUser',
    `${this.url}logotipo.png`,
    '¡Hola!',
    'Bienvenido a la familia de dinero con opciones, en esta guía aprenderás todo lo que necesitas para iniciarte como inversionista en este interesante mundo de las criptomonedas.',
    null
  ),
  new ContenidoModal(
    1,
    'newUser',
    `${this.url}wallet.png`,
    'Abriendo mi primer billetera',
    'Recuerda que en este mundo tu eres tu propio banco, tu eres responsable directo de la soberanía de tu dinero y la integridad del mismo, en la siguiente guía aprenderas los diversos tipos de billeteras existentes en el mercado y la seguridad inherente a cada una de ellas',
    null
  ),
  new ContenidoModal(
    2,
    'newUser',
    `${this.url}trans.png`,
    'Haciendo mi primera transacción',
    'Blockchain, el protocolo de la confianza, gracias a esta tecnología podemos hacer cualquier cambio de valor fungible como transacciones monetarias de manera segura y de persona a persona eliminando así los intermediarios, en la siguiente guía aprenderás a hacer una transacción a un exchange y te mostraremos los más usados',
    this.links
  ),
  new ContenidoModal(
    3,
    'newUser',
    `${this.url}exchange.png`,
    'Mi primera operación en exchange',
    'Un exchange es un sitio que te permite operar entre distintas divisas, digitales y fiat. Los exchanges son mercados financieros que permiten bajo el libre juego de oferta y demanda darle un valor económico a Bitcoin, en la siguiente guía aprenderás la operación basíca de uno, como crear ordenes de compra-venta y configurar Stop Limit',
    null
  ),
  new ContenidoModal(
    4,
    'newUser',
    `${this.url}ico.png`,
    'Mi primera inversión ICO(Oferta Inicial de moneda)',
    'Y llegamos a la mejor parte, las ofertas iniciales de monedas (PreIco - Ico), son una primer oportunidad para obtener participación de forma anticipada a proyectos que aún no salen al mercado, generando así la posible oportunidad de obtener jugosas ganancias, en esta guía aprenderás a suscribirte a la lista blanca, formularios KYC y a cargar tus monedas en billeteras compatibles con tokens ERC20 como metamask.',
    this.links
  ),
  new ContenidoModal(
    5,
    'newUser',
    `${this.url}criminal.png`,
    '¡Cuida tu dinero!',
    'Recuerda que ahí afuera hay personas inescrupulosas que quieren tu dinero, y aprovecharan la primer oportunidad para robartelo, en la ultima guía y no menos importante te enseñamos a activar factores de autenticación, uso de vpn, antilogger, creación de contraseñas seguras y almacenamiento de cripto monedas en frio (Hardware - Billeteras en papel)',
    null
  ),

];

vip:ContenidoModal[] =[ new ContenidoModal(
    0,
    'vip',
    `${this.url}corona.png`,
    'HOLA MIEMBRO VIP',
    `Los miembros VIP reciben contenido exclusivo sobre ICOs y otras criptomonedas los cuales no compartimos públicamente. El beneficio de esto es que compartimos PRE ICO los cuales dan % y bonos de inversión permitiendo así ganar más dinero en el momento que se vendan esas monedas. También recomendamos criptomonedas las cuales ya están en el mercado a buen precio antes de que suban de precio.
      gracias por tu interés, abajo encontrarás las direcciones a las cuales debes enviar $100 para VIP, recuerda incluir el fee en el monto total. Cuando envíes el pago necesito el recibo/ID de la transacción, o captura de pantalla para poder confirmar la cual debes enviarle a @Acceso Al VIP y dentro de 24-48 horas se te dará el Acceso al #vip
      En #testimonios-vip :point_left: Puedes ver o que otras personas tienen que decir acerca de nuestro servicio VIP.
      <br><br><br>
      <p class="proof">BTC:</p> 1JP37DMXetZScBNAnXrA5wU6CGMPm3QKQd<br><br>
      <p class="proof">ETH:</p> 0x04267735aBCf3B73A9898332B264BE832a66687C<br><br>
      <p class="proof">Bitcore BTX:</p> 1NTquGcBUrVXCDXjW6kb7R1fskjsat6b4g<br><br>
      <p class="proof">LTC:</p> LgL8GAJzNUyKshB3Uo6MJWG8ApvveC862P <br>`,
    null
  )];



ngOnInit(){



  if(this.contenidoModal == 'newUser'){
    this.feedTemplate = this.newUser;
    console.log(this.feedTemplate[2].links.length);
    return console.log(this.feedTemplate);
  }else if(this.contenidoModal == 'VIP'){
    console.log(this.contenidoModal);
    this.controlPosition = 'VIP';

    this.feedTemplate = this.vip;
    return console.log('estamos en vip');
  }
  // console.log(this.contenidos);
}

desplazar(destino){
  // this.paso+=1;
  this.moverHacia(destino);
  // return console.log(this.paso);

  if(this.paso > 0 && this.paso <= (this.feedTemplate.length-2)){
    return this.controlPosition = 'visible';
  }else if(this.paso < 1 ){
    return this.controlPosition = 'inicio';
  }
  return this.controlPosition = 'finaliza';
}


moverHacia(payload){
  if(payload == 'next'){
    this.traction +=20;
    return this.paso+=1;
  }
  this.traction -=20;
  return this.paso-=1;
};



enviarStatus(){
return  this.statusModalReturn.emit('desactive');
}

cerrarModal(){
return  this.contenidoModalReturn.emit('cerrado');
}

      ocultar(){
        console.log(this.paso);
        console.log(this.contenidoModal);
        this.enviarStatus();
        setTimeout(()=>{this.cerrarModal()}, 500);

        // $('#contenedorModal').css('display','none');
        // $('#modal').css('animation','animationOut .8s forwards');
        // $('#overlay').removeClass('active');
      };

}
