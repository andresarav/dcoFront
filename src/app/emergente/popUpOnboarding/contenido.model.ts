import { Multimedia } from "../../models/modelos.model";


// Modelo contenidoModal


export class ContenidoModal{
	constructor(
		public _id?:number,
		public name?: string,
		public img?: string,
		public title?: string,
		public description?: string,
		public links?: Multimedia[]
	){}
}
