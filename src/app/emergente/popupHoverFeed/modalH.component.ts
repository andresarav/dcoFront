import { Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {FeedService} from '../../feed/feed.service';
import { Update } from '../../models/modelos.model';
declare var jQuery:any;
declare var $:any;

 @Component({
   selector: "app-modalHF",
   templateUrl:"./modalH.component.html",
   styleUrls:["./modalH.component.css"],
 })

export class ModalHoverFeedComponent implements OnInit {

  constructor(
    private feedService:FeedService
  ){}

@Input() status:any;
@Output() notify:EventEmitter<string> = new EventEmitter<string>();
@Input() id:any;
news:Update[] = [new Update(
                  '0',
                  '0',
                  '0',
                  new Date(),
                  new Date(),
                  '0',
                  '0',
                )
                  ];
loading:boolean = true;
endPool:boolean;
onClick(){
  // setTimeout(()=>{this.notify.emit('CerrarPopUp');}, 500);
  this.notify.emit('CerrarPopUp');

}

cerrar(){
  return  this.status = 'inActive';
}


// VARIABLES TYPESCRIPT PARA LA CUENTA REGRESIVA --------------------------------------------------------------------------------------------------------
dias2:number;
horas2:number;
minutos2:number;
segundos2:number;
estadoPools:string;

countDown(endDate:Date){
  this.dias2 = 0;
  this.horas2 = 0;
  this.minutos2 = 0;
  this.segundos2 = 0;


  var countDownDate = new Date(endDate.getFullYear(),endDate.getMonth(),endDate.getDate()).getTime();
  var now = new Date().getTime();
  var distance = countDownDate - now;

  clearInterval(x);
  var x = setInterval(()=> {



      this.dias2 = Math.floor(distance / (1000 * 60 * 60 * 24));
      this.horas2 = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      this.minutos2 = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      this.segundos2 = Math.floor((distance % (1000 * 60)) / 1000);

      console.log(`${this.segundos2}`);
      if (distance < 0) {
          clearInterval(x);
          this.estadoPools = 'fin';
          // document.getElementById("demo").innerHTML = "FINALIZADO";
      }
  }, 1000);

}




ngOnInit(){

  var x = setTimeout(()=> {
    $(".itemUser2 img").css("width","100%");
  },1000)
  // console.log(this.NModal[0]);
// console.log(this.Datas);
// console.log(`Este es el id del proyecto   ${this.token}`);


this.feedService.getInfoProject(this.id)
      .then(res => {
        this.news = res.news;
        this.loading = false;
        // console.log(this.news);

        if(res.pool){
          const endDate = new Date(res.pool);
          const dateNow = new Date();

          if(dateNow>endDate){
            this.estadoPools = 'fin';
            console.log(`este es el estado de la pool ${this.estadoPools}`);
            return this.endPool = true;
          };
            // console.log(endDate);
            this.estadoPools = 'si';
            console.log(`este es el estado de la pool ${this.estadoPools}`);
           return this.countDown(endDate);
        }

        this.estadoPools = 'no';
        return console.log(this.estadoPools);

      });
}


}
