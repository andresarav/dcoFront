import { Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {FeedService} from '../../feed/feed.service';
import {GlobalService} from '../../global.service';



// declare var jQuery:any;
// declare var $:any;


 @Component({
   selector: "app-filterlist",
   templateUrl:"./popupFilterList.component.html",
   styleUrls:["./popupFilterList.component.css"],
 })

export class popupFilterListComponent implements OnInit {
  constructor(
    private feedService:FeedService,
    private globalService:GlobalService,
  ){}

@Input() status:any;
@Input() token:any;
@Input() dataPool:any;
@Output() notify:EventEmitter<string> = new EventEmitter<string>();
@Output() updateProject = new EventEmitter();
@Output() updateView = new EventEmitter();

@Input() proyectos:string;
indeterminate:boolean = true;
disabled:boolean = false;

onClick(){
  // setTimeout(()=>{this.notify.emit('CerrarPopUp');}, 500);
  this.notify.emit('CerrarPopUp');
}

cerrar(){
  // return  this.status = 'active';
  return  this.status = 'inActive';
}

allProjects(){
  this.proyectos = 'proyectos';
  this.indeterminate = true;
  this.disabled = false;
}

actualizoProjects(){
  this.proyectos = 'anuncios';
  this.updateProject.emit(this.proyectos);
}

updateViewPadre(view){
  this.updateView.emit(view);
}

sendlistView(){
  this.globalService.sendlistView('projects');
}

ngOnInit(){
  this.status = 'inactive';

  console.log('--------------------------------ESTE ES EL POPUP');
  console.log(this.proyectos);

// console.log(this.token);
// console.log(this.dataPool);
}


}
