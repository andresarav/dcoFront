import { Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {FeedService} from '../../feed/feed.service';
import {GlobalService} from '../../global.service';


// declare var jQuery:any;
// declare var $:any;


 @Component({
   selector: "app-userFilterList",
   templateUrl:"./popupUserFilterList.component.html",
   styleUrls:["../popupFilterList/popupFilterList.component.css"],
 })

export class popupUserFilterListComponent implements OnInit {
  constructor(
    private feedService:FeedService,
    private globalService:GlobalService

  ){}

@Input() status:any;
@Output() notify:EventEmitter<string> = new EventEmitter<string>();
proyectos:string;
usuarios:string;
indeterminate:boolean = true;
disabled:boolean = false;

onClick(){
  // setTimeout(()=>{this.notify.emit('CerrarPopUp');}, 500);
  this.notify.emit('CerrarPopUp');
}

cerrar(){
  // return  this.status = 'active';
  return  this.status = 'inActive';
}

sendlistView(payload){
  this.globalService.sendlistView(payload);
}

ngOnInit(){

  this.globalService.listViewEmitted.subscribe((res:string) => {
    this.usuarios = res;
  });
  this.status = 'inactive';

  console.log('--------------------------------ESTE ES EL POPUP');
  console.log(this.proyectos);

// console.log(this.token);
// console.log(this.dataPool);
}


}
