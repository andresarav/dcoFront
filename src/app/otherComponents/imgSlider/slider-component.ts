import { Component, ViewChild, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';

// import { MatTableDataSource, MatSort } from '@angular/material';
// import { Steemit } from './steemit.model';
// import FeedService from '../feed/feed.service';
// declare var jQuery:any;
// declare var $:any;



 @Component({
   selector: "app-slide",
   templateUrl:"./slider.component.html",
   styleUrls:["./slider.component.css"],
 	providers: []
 })

export class SlidersComponent implements OnInit  {



    constructor(public sanitizer:DomSanitizer){}

    @Input() sliderData:any;


loading:boolean = true;
transition:number = 0;
position:number = 0;
cantidad:number;
ancho:number;

convertirVideo(video){
return this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${video}`);
}
itemData:any;

    ngOnInit(){

      this.cantidad = (this.sliderData.length)-1;
      this.ancho = this.sliderData.length*100;
    }


siguiente(){
  // if(this.position<=this.cantidad){
    this.position++;
    this.transition -= 100;
  // }
}
anterior(){
  this.transition += 100;
  this.position--;

}

}
