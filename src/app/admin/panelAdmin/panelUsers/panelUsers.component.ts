import { Component, ViewChild,Output, EventEmitter, OnInit, Input} from '@angular/core';
// import { Project } from '../../proyecto/detalle-proyecto/proyecto.model';
import { NgForm } from '@angular/forms';
// import { Update } from '../../models/modelos.model';

import { FeedService } from '../../../feed/feed.service';
import {GlobalService} from '../../../global.service';
import {AuthService} from '../../../auth/auth.service';
// import { MatTableDataSource, MatSort } from '@angular/material';
// import UserService from '../../user.service';
import { Project } from '../../../proyecto/detalle-proyecto/proyecto.model';
import { User } from '../../../models/modelos.model';
// declare var jQuery:any;
// declare var $:any;

 @Component({
   selector: "add-panel-user",
   templateUrl:"./panelUsers.component.html",
   styleUrls:["../panelAdmin.component.css"],
 })

export class PanelUsersComponent implements OnInit {

  private respuesta;
  private adminVisible;

  carguita:boolean;

  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private authService: AuthService
  ){ }

  // editar:boolean;
// respuesta:string;
  loading:boolean = false;
  buttonSave:boolean = false;
  usuario:User;
  panelState;
  vista:boolean;


AcualizoPanelState(estado){
  this.panelState = estado;

  // this.envioPanelState.emit(estado);
}



editarUser(){
  this.carguita = true;
  console.log('esto es lo que envío');
  console.log(this.usuario);
  this.feedService.editUser(this.usuario)
      .subscribe(res => {
        this.carguita = false;
        // console.log(res);
        this.globalService.senduserDetail('', '', '', 'ss'); //función para cerrar panel users
        this.buttonSave = false;
        return this.globalService.openSnackBar('El usuario ha sido actualizado', 'Success');

      })
}

reciboUser(usuario){
  this.usuario.birthday = usuario.birthday;
  this.usuario.name = usuario.name;
  this.usuario.email = usuario.email;
  this.usuario.role = usuario.userRole;
  this.usuario.blocked = usuario.blocked;
  this.globalService.sendButtonSave(true);
}


reciboOpen(value){
  this.open = value;
}


open:number;
  ngOnInit(){

    this.globalService.userDetailEmitted.subscribe((res:any) => {
      console.log(res.editUser);
      //1. Ocultamos el panel admin
      this.panelState = 'ocultar';
      //2. deshabilitamos la vista para poder recargar la información en el componente
      var y = setTimeout(()=> { this.vista = false },300);


      var y = setTimeout(()=> {
        this.usuario = res.usuario;
        this.vista = res.vista;
        if(res.editUser == true){
          return this.open = 0;
        }
        return this.open = -100;
      },400);


        var x = setTimeout(()=> {
        this.panelState = res.panel;
      }, 500);

    })

    this.usuario = new User( 33234, new Date());

    //
    //
    this.globalService.buttonSaveEmitted.subscribe(res => {

      switch (res) {
        case true:
        this.buttonSave = true;
        break;
        case false:
        this.buttonSave = false;
        break;
      }

    });

  }
}
