import { Component, ViewChild, OnInit, Input, Output, EventEmitter} from '@angular/core';
// import { Project } from '../../proyecto/detalle-proyecto/proyecto.model';
import { NgForm } from '@angular/forms';
import { Ad } from '../../../publicaciones/lista.model';

import { FeedService } from '../../../feed/feed.service';
import {GlobalService} from '../../../global.service';
import {AuthService} from '../../../auth/auth.service';
// import { MatTableDataSource, MatSort } from '@angular/material';
// import UserService from '../../user.service';
// import { User } from '../user.model';
// declare var jQuery:any;
// declare var $:any;

 @Component({
   selector: "app-panel-ads",
   templateUrl:"./panelAds.component.html",
   styleUrls:["./panelAds.component.css"],
 })

export class PanelAdsComponent implements OnInit {

  private respuesta;
  private adminVisible;
  private projects;
  private panelState = 'close';

  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private authService: AuthService
  ){ }

  // editar:boolean;
// respuesta:string;
  loading:boolean = true;
  buttonSave:boolean = false;
  visibles:boolean = true;
  @Input() estadoPanel:string;
  @Input() anuncio:Ad;
  @Output() envioEstadoPanel = new EventEmitter();
  @Output() editedSend = new EventEmitter();
  @Input() visible:boolean;
  @Input() edited:string;



submit(){
  this.edited = 'editar';
}

editarProyecto(proyecto:Ad){
  this.feedService.updateNotice(proyecto)
      .subscribe(res => {
        console.log(res);
        this.globalService.openSnackBar('Proyecto editado satisfactoriamente','EDITADO');
        this.envioEstadoPanel.emit('cerrado');
      })
}

  ngOnInit(){
    console.log(this.estadoPanel);
    if(!this.anuncio){
    this.visible = false;
    }
    //
    // this.globalService.buttonSaveEmitted.subscribe(res => {
    //
    //   switch (res) {
    //     case true:
    //     this.buttonSave = true;
    //     break;
    //     case false:
    //     this.buttonSave = false;
    //     break;
    //   }
    //
    //   // this.buttonSave = res;
    // });
    //
    //
    //
    // this.globalService.loadingEmitted.subscribe(
    //   response => {
    //     this.loading = true;
    //   }
    // );
    //
    //
    // this.globalService.projectEmitted.subscribe(
    //   response => {
    //     // this.loading = true;
    //       this.loading = false;
    //       this.projects = response;
    //       this.respuesta = this.projects.title;
    //   }
    // );
    //
    //
    // this.globalService.panelAdminEmitted.subscribe(
    //   response => {
    //     this.adminVisible = response[0];
    //     this.panelState = response[1];
    //
    //     console.log('esta es la respuesta del body admin panel');
    //     console.log(response[1]);
    //   }
    // );
    //
    //
    // this.globalService.loadingEmitteds.subscribe(res => {
    //   switch (res) {
    //     case true:
    //     console.log('----------------------------------------------------RECIBIDO');
    //      this.visibles = true;
    //     console.log(this.loading);
    //     break;
    //     case false:
    //     this.visibles = false;
    //     // console.log(this.loading);
    //     break;
    //   }
    // });

  }
}
