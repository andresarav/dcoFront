import { Component, ViewChild, OnInit, Input} from '@angular/core';
// import { Project } from '../../proyecto/detalle-proyecto/proyecto.model';
import { NgForm } from '@angular/forms';
// import { Update } from '../../models/modelos.model';

import { FeedService } from '../../feed/feed.service';
import {GlobalService} from '../../global.service';
import {AuthService} from '../../auth/auth.service';
// import { MatTableDataSource, MatSort } from '@angular/material';
// import UserService from '../../user.service';
import { Project } from '../../proyecto/detalle-proyecto/proyecto.model';
// declare var jQuery:any;
// declare var $:any;

 @Component({
   selector: "app-panel-admin",
   templateUrl:"./panelAdmin.component.html",
   styleUrls:["./panelAdmin.component.css"],
 })

export class PanelAdminComponent implements OnInit {

  private respuesta;
  private adminVisible;
  private projects:Project;
  private panelState = 'close';

  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private authService: AuthService
  ){ }

  // editar:boolean;
// respuesta:string;
  loading:boolean = true;
  buttonSave:boolean = false;
  visibles:boolean = true;



// @Input() project: Project;

  // onSubmit(form:NgForm){
  //     const news = new Update(
  //       form.value.title,
  //       form.value.description,
  //       'https://www.google.com.co',
  //       new Date,
  //       new Date,
  //       this.project.id
  //     );
  //     this.project.news.unshift(news);
  //     form.reset();
  //     console.log(news);
  // }
reciboStars(stars){
this.projects.stars = stars;
this.globalService.sendButtonSave(true);

}


reciboPool(pool){
  this.projects.pool = pool;
  this.globalService.sendButtonSave(true);
}

reciboProjecto(projecto){
  // console.log('-------------------------desde PANEL ADMIN RECIBIMOS PROJECTO-----------------------------');
  this.projects.title = projecto.title;
  // this.projects.type = projecto.type;
  // this.projects.target = projecto.target;
  this.projects.bono = projecto.bono;
  this.projects.maxEth = projecto.maxEth;
  this.projects.minEth = projecto.minEth;
  this.projects.webSite = projecto.webSite;
  this.projects.whitePaper = projecto.whitePaper;
  this.projects.startDate = projecto.startDate;
  this.projects.endDate = projecto.endDate;
  this.projects.description = projecto.description;
  this.projects.team = projecto.team;
  this.projects.product = projecto.product;
  this.projects.partner = projecto.partner;
  this.projects.contractAdress = projecto.contractAdress;
  this.projects.coinName = projecto.coinName;
  this.projects.simbol = projecto.simbol;
  this.projects.usdPrice = projecto.usdPrice;
  this.projects.ethPrice = projecto.ethPrice;
  this.projects.amountPerEth = projecto.amountPerEth;
  this.projects.hardCap = projecto.hardCap;
  this.projects.totalSupply = projecto.totalSupply;
  this.projects.tokenBloq = projecto.tokenBloq;
  this.projects.bloqMessage = projecto.bloqMessage;
  console.log(this.projects);
  this.globalService.sendButtonSave(true);

}


  sendExchange(exchange){
    this.projects.exchanges = exchange;
    // this.buttonSave = true;
    this.globalService.sendButtonSave(true);
  }

  sendSocialNetwork(social){
    this.projects.socialNetworks = social;
    // this.buttonSave = true;
    this.globalService.sendButtonSave(true);
  }

  sendMultimedia(media){
    this.projects.multimedia = media;
    this.globalService.sendButtonSave(true);
    // this.buttonSave = true;
  }

  sendNotice(notice){
    this.projects.news = notice;
  }

  editarProject(){

    console.log(this.projects);
    this.feedService.editProject(this.projects)
    .subscribe(res => {
      this.globalService.panelAdminVisible('visible', 'detalle');
      this.globalService.sendButtonSave(false);
      this.globalService.openSnackBar('Proyecto editado satisfactoriamente', 'EDITADO')
    });
  }

  role:any = "admin";
  ngOnInit(){

    // if(this.authService.isLoggedIn()){
    //
    //   var user = JSON.parse(localStorage.getItem('user'));
    //   this.role = user.role;
    //   console.log(this.role);
    //   console.log("role------role------role------role------role------role------");
    //
    // }

    this.globalService.buttonSaveEmitted.subscribe(res => {

      switch (res) {
        case true:
        this.buttonSave = true;
        break;
        case false:
        this.buttonSave = false;
        break;
      }

      // this.buttonSave = res;
    });

    this.globalService.loadingEmitted.subscribe(
      response => {
        this.loading = true;
      }
    );

    this.globalService.projectEmitted.subscribe(
      response => {
        // this.loading = true;
          this.loading = false;
          this.projects = response;
          this.respuesta = this.projects.title;
      }
    );

    this.globalService.panelAdminEmitted.subscribe(
      response => {
        this.adminVisible = response[0];
        this.panelState = response[1];

        console.log('esta es la respuesta del body admin panel');
        console.log(response[1]);
      }
    );


    this.globalService.loadingEmitteds.subscribe(res => {
      switch (res) {
        case true:
         this.visibles = true;
        break;
        case false:
        this.visibles = false;
        // console.log(this.loading);
        break;
      }
    });

  }
}
