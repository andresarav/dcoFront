import { Component, ViewChild, OnInit, Input} from '@angular/core';
import { NgForm } from '@angular/forms';
import { FeedService } from '../../feed/feed.service';
import { Ad } from "../../publicaciones/lista.model";
import { ActivatedRoute, Router } from '@angular/router';
import { Multimedia } from '../../models/modelos.model';

 @Component({
   selector: "adForm",
   templateUrl:"./adForm.component.html",
   styleUrls:["./adForm.component.css"],
 })

export class adFormComponent implements OnInit {
  constructor(
    private feedService: FeedService,
    private router: Router,
  ){}

// @Input() project: Project;
links:Multimedia[]=[];

  onSubmit(form:NgForm){
      const ad = new Ad(
      form.value.title,
      form.value.user,
      form.value.description,
      'notice',
      this.links,
    );

    // const ad = [
    //   form.value.title,
    //   form.value.user,
    //   form.value.description,
    //   'notice',
    // ];

    this.feedService.addNotice(ad)
    .subscribe(
      ()=> this.router.navigate(['/']),
      error => console.log(error)
    );

  }





  ngOnInit(){ }

}
