import { Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import { Router } from '@angular/router';

 @Component({
   selector: "projectsTemplate",
   templateUrl:"./initial.component.html",
   // styleUrls:["./user.component.css"],
 	 // providers: []
 })

 export class projectsTemplateComponent implements OnInit {
   constructor(
     private authService:AuthService,
     private router:Router,
   ){}

  initComponent:string = "projects";
  role:string;


  ngOnInit(){

    this.role = this.authService.getRoleStorage();

    if(!this.authService.isLoggedIn() || this.role != 'admin'){
        return this.router.navigate(['/']);
    }
  }


}
