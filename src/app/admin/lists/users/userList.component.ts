import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {MatTableDataSource, MatDialog} from '@angular/material';
import { FeedService } from '../../../feed/feed.service';
import {GlobalService} from '../../../global.service';
import {AuthService} from '../../../auth/auth.service';
import { Router } from '@angular/router'
import { User, Membership, Subscription, Wallet, Payments } from '../../../models/modelos.model';
// import { NgForm } from '@angular/forms';
// import { FormGroup, FormControl, Validators } from "@angular/forms";


 @Component({
   selector: "user-list",
   templateUrl:"./userList.component.html",
   styleUrls:["../projectList/projectList.component.css"],
 })

export class UserListComponent implements OnInit {
  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private authService: AuthService,
    private router: Router,
    public dialog: MatDialog

  ){}

  openDialog(id) {
    const dialogRef = this.dialog.open(dialogUserDown, {  });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        console.log(`Dialog result: ${result} el id de proyecto es: ${id}`);
        this.blackListDown(id);
      }
    });
  }

  wallet:Wallet = new Wallet(
    'eliminada',
    'no dir',
    'Wallet no registrada'
  );

  // wallet:Wallet = new Wallet(
  //   'Bitcoin',
  //   '1KknGQoWsepKYMgXNpi9GjhDtvhx3m5ARF'
  // );

  payment:Payments = new Payments(
    new Date(),
    '0xc6af2734ab0c2c298780a7019b15886379039272937293f92db8d0159d54568a',
    this.wallet,
    true,
    'vip'
  );

  member:Membership = new Membership(
    new Date(),
    new Date(2018,7,10)
  );

  subscription:Subscription = new Subscription(
    'BoonTech',
    // true,
  );

  subscriptions:Subscription[] = new Array(10).fill(this.subscription); //users es un array del modelo User
  wallets:Wallet[] = new Array(10).fill(this.wallet); //users es un array del modelo User
  payments:Payments[] = new Array(10).fill(this.payment); //users es un array del modelo User


  listView:string;
  longitud:any;
  users:User[] = [];
  usersNoPay:User[] = [];
  generalUsers:User[]=[];
  blackListUsers:User[]=[];
  adminList:User[]=[];
  panelVerify:boolean = false;


  displayedColumns = ['name', 'target', 'options'];
  dataSource = new MatTableDataSource();
  dataSource2 = new MatTableDataSource();
  dataSource3 = new MatTableDataSource();
  dataSource4 = new MatTableDataSource();
  sinDatos:boolean;
  visualizarHijo:string = '';


  @Output() usuario = new EventEmitter();
  @Output() vista = new EventEmitter();
  @Output() edited = new EventEmitter();
  @Output() panelState = new EventEmitter();
  @Output() cantNoVerify = new EventEmitter();

  color:string = 'grey';

applyFilter(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  this.dataSource.filter = filterValue;
}
applyFilter2(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  this.dataSource2.filter = filterValue;
}
applyFilter3(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  this.dataSource3.filter = filterValue;
}

applyFilter4(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  this.dataSource4.filter = filterValue;
}
// editar(vista){
//   setTimeout(()=>{this.globalService.panelAdminVisible('visible', vista);}, 300);
// }

ver(){
  setTimeout(()=>{this.globalService.emitChange('detalleAdmin');}, 800);

};

epa(){

}

editarUsuario(usuario, vista, valEdited, panel){
//valEdited no esta siendo utilizado
console.log(usuario);
this.globalService.senduserDetail(usuario, vista, panel, false);
}
editarUsuario2(usuario, vista, panel){
  this.globalService.senduserDetail(usuario, vista, panel, true);
}


deleteUser(id){
  this.visualizarHijo = '';

  return this.feedService.deleteUser(id)
      .subscribe(
      () => {
        this.cargarUsuarios();
        this.globalService.openSnackBar('El Usuario se ha eliminado con exito', 'ELIMINADO');
    },
      error => this.feedService.handleError(error)
    );
}


borrarUsuario(id) {
  const dialogRef = this.dialog.open(dialogUserDelete, {  });

  dialogRef.afterClosed().subscribe(result => {
    if(result){
      console.log(`Dialog result: ${result} el id de proyecto es: ${id}`);
      this.deleteUser(id);
    }
  });
}



idRandom(){
  return Math.floor((Math.random() * 10) + 1);
};


blackListDown(userId){

  this.feedService.verifyMembership(userId)
            .subscribe(res => {

                  console.log(res);
                  this.globalService.sendListUpdate('');

            });

}

cargarUsuarios(){

this.adminList= [];
this.blackListUsers= [];
this.generalUsers= [];
this.users= [];
this.usersNoPay= [];


  this.feedService.getUserList()
      .then((res:any[]) => {
            // this.users = res;
            // if(this.users.length == 0){
            //   return this.sinDatos = true;
            // }

            console.log(res);

            res.map( (user) => {

              let userc = new User(
                user.id,
                user.birthday,
                user.name,
                user.username,
                user.wallets ,
                user.payments,
                user.email,
                this.authService.getRole(user.roleId, user.id),
                user.subscriptions,
                true,
                // false,
                user.txAudit,
                this.member,
                user.password,
                this.authService.parseProfileImage(user.image),
                user.createdAt,
                10,
                user.extra.passedDays,
                user.extra.percentagePassed,
                user.extra.totalDays,
                user.verifyMembership
              );

                console.log(userc);

              if(userc.txAudit == true){
                // ALIMENTAMOS LA LISTA DE USUARIOS PENDIENTES DE VERIFICACIÓN
                this.globalService.sendNoVerify(false)
                this.usersNoPay.push(userc);
              }

              if(userc.txAudit == false && userc.role == 'vip' && userc.verifyMembership == false){
                // ALIMENTAMOS LA LISTA DE USUARIOS VIP Y PAGOS AL DÍA
                this.users.push(userc);
              }

              if(userc.role == 'user' && userc.verifyMembership == false){
                // ALIMENTAMOS LA LISTA DE USUARIOS GRATUITOS CON ACCESO LIMITADO
                  this.generalUsers.push(userc);
              }

              // if(userc.verifyMembership == true && userc.role == 'user' && userc.txAudit == false){
              if(userc.verifyMembership == true && userc.role == 'user' && userc.txAudit == false){
                // ALIMENTAMOS LA LISTA NEGRA DE USUARIOS QUE DEBEN SER DADOS DE BAJA
                this.blackListUsers.push(userc);
              }

              if(userc.role == 'admin'){
                // ALIMENTAMOS LA LISTA DE USUARIOS ADMINISTRADORES
                this.adminList.push(userc);
              }
            });

            if(this.usersNoPay.length == 0){
              this.panelTam = 0;
            }else if(this.usersNoPay.length == 1){
              this.panelTam = 50;
            }else if(this.usersNoPay.length == 2){
              this.panelTam = 100;
            }else if(this.usersNoPay.length > 2){
              this.panelTam = 150;
            }



          this.cantNoVerify.emit(this.usersNoPay.length);
          this.globalService.sendlistView('vip');
          if(this.blackListUsers.length > 0){
            this.globalService.sendlistView('black');
          }


          // console.log(this.users);
            // console.log(this.usersNoPay);
            this.dataSource = new MatTableDataSource(this.users);
            this.dataSource2 = new MatTableDataSource(this.generalUsers);
            this.dataSource3 = new MatTableDataSource(this.blackListUsers);
            this.dataSource4 = new MatTableDataSource(this.adminList);
            this.visualizarHijo = 'users';

      })

  // this.feedService
  // .getProjectsADMIN()
  // .then((projects:any[])=> {
  //   this.projects = projects;
  //   // this.loading = false;
  //   this.dataSource = new MatTableDataSource(this.projects);
  //   this.visualizarHijo = 'projects';
  //   console.log(this.projects);
  // });
}
panelTam:any;
  ngOnInit(){
    if(!this.authService.isLoggedIn()){
      return this.router.navigate(['/auth']);
      // return this.globalService.sendLoadings(false);
    }
    this.cargarUsuarios();
    this.globalService.panelAdminVisible('visible', 'l');
    this.globalService.emitChange('no');

    this.globalService.sendLoading();
    this.globalService.noVerifyListEmitted.subscribe(res => {

      //validamos si hay usuarios en lista no verificada emitimos el estado al boton de usuarios verificados al componente padre en admintemplate
      if(res == true && this.panelVerify == true){
        this.globalService.sendopenVerifyList(false);
        return this.panelVerify = false;
      }
      this.globalService.sendopenVerifyList(true);
      return this.panelVerify = true;
      // switch(res){
      //   case true
      // }
    });

    this.globalService.listViewEmitted.subscribe((res:string) => {
      this.listView = res;
    });

    this.globalService.listUpdateEmitted.subscribe((res:string) => {
      this.visualizarHijo = "";

      var x = setTimeout(()=> {  this.cargarUsuarios(); },500)

    })

    // setTimeout(()=>{this.globalService.sendLoading(true);}, 300);

  }


}


@Component({
  selector: 'dialogos',
  templateUrl: '../../../emergente/dialogList/dialogoDown.html',
  styleUrls:["../../../emergente/dialogList/dialogos.css"],
})

export class dialogUserDown {}


@Component({
  selector: 'dialogo-delete',
  templateUrl: '../../../emergente/dialogList/dialogoDelete.html',
  styleUrls:["../../../emergente/dialogList/dialogos.css"],
})

export class dialogUserDelete {}
