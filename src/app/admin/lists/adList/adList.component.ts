import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {MatTableDataSource, MatDialog} from '@angular/material';
import { FeedService } from '../../../feed/feed.service';
import {GlobalService} from '../../../global.service';
import {AuthService} from '../../../auth/auth.service';
import { Router } from '@angular/router'
import { Ad } from '../../../publicaciones/lista.model';
// import { NgForm } from '@angular/forms';
// import { FormGroup, FormControl, Validators } from "@angular/forms";


 @Component({
   selector: "ads-list",
   templateUrl:"./adList.component.html",
   styleUrls:["../projectList/projectList.component.css"],
 })

export class adverListComponent implements OnInit {
  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private authService: AuthService,
    private router: Router,
    public dialog: MatDialog
  ){}


  openDialog(id) {
    const dialogRef = this.dialog.open(DialogAdList, { });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        console.log(`Dialog result: ${result} el id de proyecto es: ${id}`);
        this.borrarProyecto(id);
      }
    });
  }

  displayedColumns = ['name', 'target', 'options'];
  dataSource = new MatTableDataSource();
  ads:Ad[];
  sinDatos:boolean;
  visualizarHijo:string = '';
  @Output() anuncio = new EventEmitter();
  @Output() vista = new EventEmitter();
  @Output() edited = new EventEmitter();


applyFilter(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  this.dataSource.filter = filterValue;
}

editar(vista){
  setTimeout(()=>{this.globalService.panelAdminVisible('visible', vista);}, 300);
// this.globalService.panelAdminVisible('visible', vista);
}

ver(){
  setTimeout(()=>{this.globalService.emitChange('detalleAdmin');}, 800);

};

editarAnuncio(anuncio, vista, valEdited){
  // console.log(anuncio);
  // console.log(vista);
this.anuncio.emit(anuncio);
this.vista.emit(vista);
this.edited.emit(valEdited);
}


borrarProyecto(id){
  this.visualizarHijo = '';


  return this.feedService.deleteNotice(id)
      .subscribe(
      () => {
        this.cargarAnuncios();
        this.globalService.openSnackBar('El Anuncio se ha eliminado con exito', 'ELIMINADO');
    },
      error => this.feedService.handleError(error)
    );
}


cargarAnuncios(){

  this.feedService.getNotices()
      .then((res:Ad[]) => {
            this.ads = res;
            this.dataSource = new MatTableDataSource(this.ads);
            this.visualizarHijo = 'ads';
            if(this.ads.length == 0){
              return this.sinDatos = true;
            }
            console.log(res);
      })

  // this.feedService
  // .getProjectsADMIN()
  // .then((projects:any[])=> {
  //   this.projects = projects;
  //   // this.loading = false;
  //   this.dataSource = new MatTableDataSource(this.projects);
  //   this.visualizarHijo = 'projects';
  //   console.log(this.projects);
  // });
}

  ngOnInit(){
    if(!this.authService.isLoggedIn()){
      return this.router.navigate(['/auth']);
      // return this.globalService.sendLoadings(false);
    }
    this.cargarAnuncios();
    this.globalService.panelAdminVisible('visible', 'l');
    this.globalService.emitChange('no');

    this.globalService.sendLoading();
    // setTimeout(()=>{this.globalService.sendLoading(true);}, 300);

  }


}


@Component({
  selector: 'dialogos',
  templateUrl: '../../../emergente/dialogList/dialogos.html',
  styleUrls:["../../../emergente/dialogList/dialogos.css"],
})

export class DialogAdList {}
