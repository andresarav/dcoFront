import { Component, OnInit, Input, Inject} from '@angular/core';
import {MatTableDataSource, MatDialog} from '@angular/material';
import { Project } from '../../../proyecto/detalle-proyecto/proyecto.model';
import { FeedService } from '../../../feed/feed.service';
import {GlobalService} from '../../../global.service';
import {AuthService} from '../../../auth/auth.service';
// import { NgForm } from '@angular/forms';
// import { FormGroup, FormControl, Validators } from "@angular/forms";


 @Component({
   selector: "project-list",
   templateUrl:"./projectList.component.html",
   styleUrls:["./projectList.component.css"],
 })


export class ProjectListComponent implements OnInit {
  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private authService: AuthService,
    public dialog: MatDialog
  ){}



  openDialog(id) {
    const dialogRef = this.dialog.open(DialogContentExampleDialog, { });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        console.log(`Dialog result: ${result} el id de proyecto es: ${id}`);
        this.borrarProyecto(id);
      }
    });
  }
  displayedColumns = ['name', 'target', 'options'];
  dataSource = new MatTableDataSource();
  projects:Project[];
  visualizarHijo:string = '';

applyFilter(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  this.dataSource.filter = filterValue;
}

editar(vista){
  setTimeout(()=>{this.globalService.panelAdminVisible('visible', vista);}, 300);
// this.globalService.panelAdminVisible('visible', vista);
}

ver(){
  setTimeout(()=>{this.globalService.emitChange('detalleAdmin');
}, 800);
// this.globalService.emitChange('detalleAdmin');


};
// @Input() project: Project;
//
// signupForm: FormGroup;
//   onSubmit(form:NgForm){
//       // const news = new Update(
//       //   form.value.title,
//       //   form.value.description,
//       //   'https://www.google.com.co',
//       //   new Date,
//       //   new Date,
//       //   this.project.id
//       // );
//       // this.project.news.unshift(news);
//       // form.reset();
//       // console.log(news);
//
//       console.log(form.value);
//   }



borrarProyecto(id){
  this.visualizarHijo = '';


  return this.feedService.borrarADMIN(id)
      .subscribe(
      () => {
        this.cargarProyectos();
        this.globalService.openSnackBar('El proyecto se ha eliminado con exito', 'ELIMINADO');

    },
      error => this.feedService.handleError(error)
    );
}


cargarProyectos(){
  this.feedService
  .getProjectsADMIN()
  .then((projects:any[])=> {
    this.projects = projects;
    // this.loading = false;
    this.dataSource = new MatTableDataSource(this.projects);
    this.visualizarHijo = 'projects';
    console.log('Lista proyectos');
    console.log(this.projects);
  });
}

  ngOnInit(){

    this.cargarProyectos();
    this.globalService.panelAdminVisible('visible', 'l');
    this.globalService.emitChange('no');

    this.globalService.sendLoading();
    // setTimeout(()=>{this.globalService.sendLoading(true);}, 300);

  }
}



@Component({
  selector: 'dialogos',
  templateUrl: '../../../emergente/dialogList/dialogos.html',
  styleUrls:["../../../emergente/dialogList/dialogos.css"],
})

export class DialogContentExampleDialog {}
