import { Component, ViewChild, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { NgForm } from '@angular/forms';
import {FeedService} from '../../../feed/feed.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User, Subscription, Payments, Wallet, Membership  } from "../../../models/modelos.model";
import {GlobalService} from '../../../global.service';
import {AuthService} from '../../../auth/auth.service';
// import {Sort} from '@angular/material';

declare var jQuery:any;
declare var $:any;

 @Component({
   selector: "addUserComponent",
   templateUrl:"./addUserComponent.component.html",
   styleUrls:["../../projectForm/addComponent/addComponent.component.css"],
 })

export class AddUserFormComponent implements OnInit {

  // sweetScroll: SweetScroll;


  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private authService: AuthService,
    private router: Router,

  ){
    // this.sweetScroll = new SweetScroll();
  }



// Componente user----------------------------------------------------_____________________________----

@Input() user: User;
@Input() visualizar:string;

suscripciones:Subscription[]=[];
payments:Payments[]=[];
billeteras:Wallet[]=[];

carguita:boolean = false;
selected:any = 'Ethereum';
walletPredet:string;
walletNamesPred:string ="Ver Wallet";


estadoMem:boolean;


@Input()  formsource:string;
itemActiveAncho:number = 25;
stars:number = 1;
bloqtoken:number = 0;
@Input() open:number;
seccion:string;
@Input() traction:number;
progressM:number;
rol:string;
abrirRetractil:boolean;


@Output() sendMembership = new EventEmitter();
@Output() sendUser = new EventEmitter();
@Output() sendWallet = new EventEmitter();
@Output() sendPayment= new EventEmitter();
@Output() sendtxAudit= new EventEmitter();
@Output() sendOpen= new EventEmitter();


// noticias:Update[];

crearPiscina(){
  document.getElementById('crearPool').click();
}

renovarMem(){

}

changeSave(){
  document.getElementById('enviarUsuario').click();
}


predeterminados(wallet){
  console.log(wallet.name);
  this.walletPredet = wallet.address;
  this.selected = wallet.type;
  this.walletNamesPred = wallet.name;
  this.idWallet = wallet.id;
}

idWallet:any;
tipoWallet:string;
walletAdress:string;
walletName:string;
WalletId:string;
payRole:string = 'asss';

fechaAdd:Date;
verifyIdPayment:any;


mostrarPago(pay){
  this.tipoWallet = pay.wallet.type;
  this.walletName = pay.wallet.name;
  this.walletAdress = pay.wallet.address;
  this.WalletId = pay.txId;
  this.payRole = pay.type;
  this.verifyIdPayment = pay.id;

  // console.log('este es el pago');
  // console.log(pay);


    this.estadoMem = true;
    const fecha = new Date();
    const entrega = new Date();
    const dia = fecha.getDate();
    const mes = fecha.getMonth()+1;// +1 porque los meses empiezan en 0
    const anio = fecha.getFullYear();
    entrega.setDate(entrega.getDate() + 32);
    this.fechaAdd = entrega;

}

aumentarFecha(){
  this.estadoMem = false;
}

role:string;
getRole(){
  this.role = this.authService.getRoleStorage();
}

userId:any;
getIdUser(){
  // const user = localStorage.getItem('user');
  const { id } = JSON.parse(sessionStorage.getItem('user'));
  return this.userId = id;
}

visualizar2:string;
usuarioWallet(){
  if(this.user.id != this.getIdUser()){
    //función para dejar visible el cta de nueva billetera solo para su propio usuario
    return this.visualizar2 = "billeteras";
  }
}


//FUNCIONES PARA CREAR ELEMENTO DE MODELOS PAYMENTS, WALLETS, MEMBERSHIP USER, CAMBIO DE ROL -------------------------------

roleChange(role){
  if(role == 'vip'){
    this.feedService.roleChangeVip(this.user.id)
        .subscribe(res => {
          console.log(res);
          this.carguita=false;
          this.globalService.sendListUpdate('');
        })
  }
}


userChange(form:NgForm){


  const usuario = new User(
    null,
    form.value.birthday,
    form.value.name,
    null,
    null,
    null,
    form.value.email,
    form.value.role,
    null,
    false,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    'not'
  );

  if(this.infoPerfil == 'miPerfil' ){
    this.user.birthday = usuario.birthday;
    this.user.name = usuario.name;
    this.user.email = usuario.email;
    this.user.blocked = usuario.blocked;
    // return this.editarUsuario();
  }

  this.sendUser.emit(usuario);
}


editarUsuario(){
  this.carguita = true;
  this.feedService.editUser(this.user)
      .subscribe(res => {
        this.carguita = false;
        this.perfilEdited = false;
        sessionStorage.setItem('user', JSON.stringify(this.user));
        return this.globalService.openSnackBar('El usuario ha sido actualizado', 'Success');
      })
}


newPay(form:NgForm){

  const walletUser = new Wallet(
    form.value.type,
    form.value.address,
    this.walletNamesPred,
    null,
    this.idWallet
  );

  const pay = new Payments(
    null,
    form.value.txId,
    walletUser,
    false,
    'vip',
    this.idWallet,
    this.user.id
  );

  if(!form.value.type || !form.value.address || !form.value.txId ){
    return this.globalService.openSnackBar('Faltan campos por completar', 'Error');
  }

  this.carguita=true;

  if(this.role == "admin"){
    // 'se hace el pago desde usuario  admin'
      this.feedService.addPaymentByAdmin(pay, this.user.id)
        .subscribe(res =>{
          pay.id = res.id;
          pay.createdAt = res.createdAt;
          this.ingresarPago(pay); //ingresamos el pago al array
          this.carguita=false;
          this.globalService.sendListUpdate('');
          this.general(); //dirigimos al usuario a verificar membresía inmediatamente se crea el pago
          return this.globalService.openSnackBar('El pago se ha creado con exito.', 'Administrador');
        },
      error => {
        this.carguita=false;
        return this.globalService.openSnackBar('Ups, algo anda mal, intenta luego.', 'Error');
        }
      )
  }
  // SE GENERA UN PAGO COMO USUARIO ADMINISTRADOR
  if(this.role != "admin"){
      this.feedService.addPayment(pay)
          .subscribe(res => {
            pay.createdAt = res.createdAt;
            pay.id = res.id;
            this.ingresarPago(pay); //ingresamos el pago al array
            this.carguita=false;
            this.globalService.sendListUpdate('');
            this.general(); //dirigimos a la info de usuario
            return this.globalService.openSnackBar('El pago se ha creado con exito, el pago será verificado por administración, se paciente', 'Usuario');
          },
          error => {
            this.carguita=false;
            this.globalService.openSnackBar('No lo creo bro, hay un pago pendiente sin verificar.', 'Error')});
  }

  form.reset();


  // this.sendExchage.emit(this.exchanges);
  }

ingresarPago(pay){
  this.payments.unshift(pay);
  this.walletNamesPred ="Ver Wallet explorer";
  // this.sendPayment.emit(this.payments);
  return this.sendtxAudit.emit(false);
}




newWallet(form:NgForm){
  const wallet = new Wallet(
    form.value.type,
    form.value.address,
    form.value.name
  );
  console.log('MODELO ENVIADO: ');
  console.log(wallet);

  if(!form.value.type || !form.value.address || !form.value.name){
      return this.globalService.openSnackBar('Faltan campos por completar', 'Error');
  }
  this.carguita=true;
  this.feedService.addWallet(wallet)
    .subscribe(res => {
      this.carguita=false;
      wallet.id = res.id;
      this.billeteras.unshift(wallet);
      form.reset();
      this.globalService.openSnackBar('Nueva wallet creada', 'exito');
      // return this.sendWallet.emit(this.billeteras);
    }, (error) => this.globalService.openSnackBar('No se pudo crear la wallet', 'ERROR D:') );


    // console.log(this.billeteras);
    // this.sendMultimedia.emit(this.multimedias);
    // console.log(this.multimedias);
  }



membershipUpdate(form:NgForm){

  // console.log(form.value.endDateM);
  if(!form.value.endDateM){
    // console.log('fecha predeterminada')
    form.value.endDateM = this.fechaAdd;
    // console.log(form.value.endDateM)
  }

  if(!form.value.pays || !form.value.endDateM ){
      return this.globalService.openSnackBar('Faltan campos por completar', 'Error');
  }

  this.carguita=true;

  console.log(this.user.userName);
  console.log(this.user.id);


  this.feedService.verifyPayment(this.verifyIdPayment, this.user.id, form.value.endDateM)
          .subscribe(res => {
            console.log(res);

            this.feedService.verifyMembership(this.user.id)
                    .subscribe(res => {
                      this.roleChange('vip');
                      // this.carguita=false;
                      // this.globalService.sendListUpdate('');
                      this.globalService.senduserDetail('', '', '', 'ss'); //función para cerrar panel users
                      return this.globalService.openSnackBar('Pago y membresía verificada', 'Exito');
                    })
        });



}




  //FUNCIÓN ELIMINAR ITEM EN CUALQUIERA DE LAS LISTAS --------------------------

  eliminarItem(social, payload, id){
    // console.log(payload);
    // console.log(social);
    console.log(payload);
    switch (social) {
      case 'historial':
        this.carguita=true;
        if(this.role != 'admin'){

          this.feedService.deletePayment(id)
              .subscribe(res => {
                this.carguita=false;
                for (let i = 0; i < this.payments.length; i++) {
                  if(this.payments[i].txId == payload){
                     this.payments.splice(i,1);
                     this.globalService.sendListUpdate('');
                     this.globalService.senduserDetail('', '', '', 'ss'); //función para cerrar panel users
                     return this.globalService.openSnackBar('Pago eliminado exitosamente', 'Success');
                  }
                }

              },
              error => {
                this.carguita=false;
                this.globalService.openSnackBar('Ups, no te pases de listo querido, no tienes derecho para esto', 'Error')}
            )
        }else if(this.role == 'admin'){

        this.feedService.deletePaymentByAdmin(id)
            .subscribe(res => {
              this.carguita=false;
              for (let i = 0; i < this.payments.length; i++) {
                if(this.payments[i].txId == payload){
                   this.payments.splice(i,1);
                   this.globalService.sendListUpdate('');
                   this.globalService.senduserDetail('', '', '', 'ss'); //función para cerrar panel users
                   return this.globalService.openSnackBar('Pago eliminado exitosamente', 'Success');
                }
              }

            },
            error => {
              this.carguita=false;
              this.globalService.openSnackBar('Lo siento bro, en otra ocasión será.', 'Error')}
          );
        }


      break;
      case 'billetera':
      this.carguita=true;

          this.feedService.deleteWallet(id)
            .subscribe(res => {
              this.carguita=false;
              for (let i = 0; i < this.billeteras.length; i++) {
                if(this.billeteras[i].address == payload){
                   this.billeteras.splice(i,1);
                   this.globalService.openSnackBar('Billetera eliminada satisfactoriamente', 'Success');
                }
              }
            },
          (error) => {this.globalService.openSnackBar('Ups, no te pases de listo querido, no tienes derecho para esto', 'Ups! :(')
          this.carguita=false;
        } );
      break;
    }
  }

  abrirAnadidor(){
    this.open = 0;
    this.sendOpen.emit(0);
  }
  anadir(){
      this.open = -100;
      this.sendOpen.emit(-100);
  }
  cancelar(){
    this.open = -100;
  }

  noticia(){
    this.seccion = 'a Noticia';
    this.traction = 0;
    this.visualizar = 'noticia';
    this.visualizar2 = 'noticia';
  }

  suscripcion(){
    this.visualizar = 'suscripcion';
    this.visualizar2 = 'suscripcion';

      switch (this.formsource) {
        case 'created':
          this.traction = 75;
          break;
      }
  }



  history(){
    this.seccion = 'o pago';
    this.visualizar = 'historial';
    this.visualizar2 = 'historial';
    switch (this.formsource) {
      case 'created':
        this.traction = 50;
        break;

    }
  }

  billetera(){
    this.seccion = 'a Billetera';
    this.visualizar = 'billeteras';

    this.usuarioWallet();

    switch (this.formsource) {
      case 'created':
        this.traction = 25;
        break;

    }
  }

  general(){
    // this.seccion = 'a Billetera';
    this.visualizar = 'general';
    this.visualizar2 = 'general';

    switch (this.formsource) {
      case 'created':
        this.traction = 0;
        break;

    }
  }

// Role:string;
caduca:number;
colorFondo:any;
userStorage:User;
infoPerfil:string;
perfilEdited:boolean = false;

  ngOnInit(){

    console.log('--------------------------------------------------------');
    console.log(this.user.payments);
    console.log('--------------------------------------------------------');

    this.userStorage = this.authService.getUserStorage();
    // console.log('this.userthis.userthis.userthis.userthis.userthis.userthis.userthis.userthis.user');
    // console.log(this.user);
    this.caduca = this.user.totalDays - this.user.passedDays;
    // this.Role = this.feedService.getRoleStorage();
    this.getRole();

    this.suscripciones = this.user.subscriptions;
    this.payments = this.user.payments;
    this.billeteras = this.user.wallets;

    if(this.userStorage.id == this.user.id){
      //si los dos ids coinciden es porque estamos accediendo a nuestro propio perfil, por ende nos debe mostrar un template diferente
      this.infoPerfil = 'miPerfil';
    }

    // this.multimedias = this.projects.multimedia;
    // this.exchanges = this.projects.exchanges;
    // this.redeSociales = this.projects.socialNetworks;
    // console.log(this.projects);
    // this.news = this.projects.news;
    // if(this.projects.pool){
    // this.piscinActual = this.projects.pool;
    // }

    // news:Update[] = [];




    this.visualizar = 'general';
    // this.progressM = this.user.percentagePassed;

    if(this.user.percentagePassed > 0){
      this.colorFondo = '#8BC34A';
    }if(this.user.percentagePassed > 50){
        this.colorFondo = '#cddc39';
      }if(this.user.percentagePassed > 80){
          this.colorFondo = '#ff9800';
    }

this.globalService.abrirAnadidEmitted.subscribe((res:boolean) => {

  switch(res){
    case true:
    // setTimeout(()=>{this.abrirAnadidor(); alert('a la mierda')}, 3000);

      this.general();
      this.abrirAnadidor();
    break;
    case false:
      this.cancelar();
    break;
  }


})





  }


}
