import { userTemplateComponent } from './initi/user.component';
import { projectsTemplateComponent } from './initi/projects.component';

export const ADMIN_ROUTES = [
  { path: '', component: projectsTemplateComponent },
  { path: 'users', component: userTemplateComponent },
];
