import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FeedService} from '../feed/feed.service';
import {GlobalService} from '../global.service';
import { Ad } from '../publicaciones/lista.model';
import { User } from '../models/modelos.model';


 @Component({
   selector: "adminTemplate",
   templateUrl:"./adminTemplate.component.html",
   styleUrls:["./adminTemplate.component.css"],
 })

export class AdminComponent implements OnInit {
  constructor(private feedService: FeedService,
    private globalService: GlobalService,
  ){}

  @Input() visualizarPadre:string;
  // @Output() controlAdmin = new EventEmitter();
  estadoPanel:string = 'lista';
  status:string;
  proyectos:string = 'proyectos';
  usuarios:string = 'vip';
  anuncio:Ad;
  estadoPanelAd:string = 'cerrado';
  enviarVisible:boolean;
  edited:string;
  usuario:User;
  vista:boolean;
  userVerify:boolean = false;
  cantNoVerify:number;
// @Input() project: Project;

habilitoVistaPanel(vista){
  this.vista = vista;
}

reciboUser(usuario){
  this.usuario=usuario;
}

ActualizoPanelState(estado){
  this.estadoPanelAd = estado;
}

reciboEstadoPanelAd(estado){
  this.estadoPanelAd = estado;
  this.edited = 'falso';
}
reciboCantNoVerify(cant){
    this.cantNoVerify = cant;
}

reciboEdited(val){
  this.edited = val;
  console.log(val);
}

reciboAd(anuncio){
  this.anuncio = anuncio;
  this.enviarVisible = true;
  console.log(this.anuncio);
}

actualizoProjects(lista){
  this.proyectos = lista;
}

tractionMenu:number;
typeCreated:string = "algo";

pantalla(payload, text){
  this.visualizarPadre = payload;
  console.log(this.visualizarPadre);
  this.tractionMenu = -12;
  this.tractionItemList = -50;
  this.typeCreated = text;
}

volver(){
  this.visualizarPadre = 'projects';
  this.tractionItemList = 50;
  this.tractionMenu = 25;
  this.estadoPanel = 'lista';
}

enviarProject(){
   this.proyectos = 'proyectos';
   console.log(this.proyectos);
}


onClick(){
  console.log('desde hijo');
  // this.controlAdmin.emit('CerrarPopUp');
}

abrirFiltro(){
  return this.status = 'active';
   // this.status = 'active';
   // console.log(this.status);
}

recibeStatus(status){
  return this.status = status;
}

updateViewPadre(view){
  this.visualizarPadre = view;
}

vistActual:string;
tractionItemList:number;
  ngOnInit(){

    this.feedService.FuncionScrollDetail(false);

    this.globalService.listViewEmitted.subscribe((res:string)=>{
      this.vistActual = res;
      switch(res){

        case 'vip':
        this.tractionItemList = 80;
        break;
        case 'general':
        this.tractionItemList = 25;
        break;
        case 'black':
        this.tractionItemList = -25;
        break;
        case 'admin':
        this.tractionItemList = -80;
        break;
        case 'projects':
        this.tractionItemList = 50;
        break;
        case 'ads':
        this.tractionItemList = 0;
        break;
      }

    });

    this.globalService.openVerifyListEmitted.subscribe(res => {
      switch(res){
        case true:
        this.userVerify = true;
        break;
        case false:
        this.userVerify = false;
        break;
      }
    })

  }

}
