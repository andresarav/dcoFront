import { Component, ViewChild, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { NgForm } from '@angular/forms';
import {FeedService} from '../../../feed/feed.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from "../../../proyecto/detalle-proyecto/proyecto.model";
import { Exchange, Update, Social, Multimedia, Pool } from "../../../models/modelos.model";
import {GlobalService} from '../../../global.service';
// import {Sort} from '@angular/material';

 @Component({
   selector: "editComponent",
   templateUrl:"./editComponent.component.html",
   styleUrls:["./editComponent.component.css"],
 })

export class EditComponent implements OnInit {
  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private router: Router,
  ){}



@Output() projecto = new EventEmitter();
@Output() sendStars = new EventEmitter();
@Input() projects: Project;
stars:number;

  submit(){
    document.getElementById('epalape').click();
  }

  onSubmit(form: NgForm){
    // console.log(form.value);
    this.projecto.emit(form.value)
  }
  star(stars){
    this.stars = stars;
    this.sendStars.emit(stars);

  };

  ngOnInit(){
    this.stars = this.projects.stars;
    // console.log(this.projects);


  }


}
