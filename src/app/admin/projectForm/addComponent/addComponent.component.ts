import { Component, ViewChild, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { NgForm } from '@angular/forms';
import { FeedService } from '../../../feed/feed.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from "../../../proyecto/detalle-proyecto/proyecto.model";
import { Exchange, Update, Social, Multimedia, Pool } from "../../../models/modelos.model";
import {GlobalService} from '../../../global.service';
// import {Sort} from '@angular/material';

 @Component({
   selector: "addComponent",
   templateUrl:"./addComponent.component.html",
   styleUrls:["./addComponent.component.css"],
 })

export class AddFormComponent implements OnInit {
  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private router: Router,
  ){}

  exchan = [
    'binance',
    'idex',
    'ether Delta',
    'bittrex',
    'hitBTC',
    'poloniex',
    'bitfinex',
    'huobi',
    'oKEx',
    'bithumb',
    'kraken',
    'bitstamp',
    'bit-Z',
    'bibox'
   ];


   socials = [
     'facebook',
     'twitter',
     'youtube',
     'telegram',
     'reddit',
     'discord',
     'bitcointalk',
     'github',
     'medium',
    ];



    medias = [
      'Video',
      'Imagen',
     ];


verlo:any;
@Input() projects: Project;
@Input()  formsource:string;
itemActiveAncho:number;
stars:number = 1;
bloqtoken:number = 0;
open:number;
seccion:string;
visualizar:string;
traction:number = 0;
piscina:boolean;
// private projects;

//arreglos modelos de las secciones
// exchanges:Exchange[];
exchanges:Exchange[] = [];
redeSociales:Social[] = [];
multimedias:Multimedia[] = [];
news:Update[] = [];
piscinActual:Pool = new Pool();


@Output() sendExchage = new EventEmitter();
@Output() sendSocialNetwork = new EventEmitter();
@Output() sendMultimedia = new EventEmitter();
@Output() sendNotice = new EventEmitter();
@Output() sendPool = new EventEmitter();


// noticias:Update[];

crearPiscina(){
  document.getElementById('crearPool').click();
}



//FUNCIONES PARA CREAR ELEMENTO DE MODELOS MULTIMEDIA, EXCHANGE, RED SOCIAL -------------------------------
newNotice(form:NgForm){
  const noticia = new Update(
    form.value.title,
    form.value.description,
    form.value.link,
    new Date(),
    null,
    this.projects.id
  );

  this.feedService.addNew(noticia)
  .subscribe(
    (res)=> {
      // this.router.navigate(['/'])
      console.log(res);
      noticia.id = res.id;
      this.news.push(noticia);
      form.reset();
      // this.globalService.sendNotice(noticia);
      this.sendNotice.emit(this.news);
      this.globalService.openSnackBar('Noticia creada con exito', ':D');

    },
    error => console.log(error)
  );


}


newPool(form:NgForm){
  const pool = new Pool(
    form.value.warning,
    form.value.minEth,
    form.value.maxEth,
    form.value.link,
    form.value.endDate,
    true,
    false,
    form.value.bono
  );

  if(!form.value.endDate || !form.value.warning || !form.value.minEth || !form.value.maxEth || !form.value.link || !form.value.bono){
    return console.log('Aun te faltan campos');
  }

  this.piscinActual = pool;
  this.sendPool.emit(pool);



  // this.feedService.addPool(pool)
  // .subscribe(
  //   ()=> {
  //     alert('Pool creada con exito');
  //   },
  //   error => console.log(error)
  // );


}





newExchange(form:NgForm){

  const exchange = new Exchange(
    form.value.nombre,
    form.value.link,
    false
  );

  if(!form.value.link || !form.value.nombre){
    return console.log('No has ingresado Exchange');
  }

  this.exchanges.unshift(exchange);
  form.reset();
  this.sendExchage.emit(this.exchanges);

}

newSocial(form:NgForm){
  const social = new Social(
    form.value.socialname,
    form.value.link
  );

  if(!form.value.link || !form.value.socialname){
    return console.log('No has ingresado link');
  }

  this.redeSociales.push(social);
  form.reset();
  this.sendSocialNetwork.emit(this.redeSociales);

  // console.log(this.redeSociales);
}

newMultimedia(form:NgForm){
  const medial = new Multimedia(
    form.value.link,
    form.value.multiName
  );

  if(!form.value.link || !form.value.multiName){
    return console.log('No has ingresado link');
  }

  this.multimedias.push(medial);
  form.reset();
  this.sendMultimedia.emit(this.multimedias);
  // console.log(this.multimedias);
}






  //Función nodriza para eliminarItem(social, payload)...
  // eliminarNodriza(arreglo, payload){
  //   console.log('eliminando....')
  //   for (let i = 0; i < arreglo.length; i++) {
  //     if(arreglo[i].link == payload){
  //       return arreglo.splice(i,1);
  //     }
  //   }
  // }



  //FUNCIÓN ELIMINAR ITEM EN CUALQUIERA DE LAS LISTAS --------------------------

  eliminarItem(social, payload){
    // console.log(payload);
    // console.log(social);
    console.log(payload);
    switch (social) {
      case 'social':
          for (let i = 0; i < this.redeSociales.length; i++) {
            if(this.redeSociales[i].link == payload){
               this.redeSociales.splice(i,1);
               return this.sendSocialNetwork.emit(this.redeSociales);
            }
          }

      break;
      case 'exchange':
          for (let i = 0; i < this.exchanges.length; i++) {
            if(this.exchanges[i].link == payload){
              this.exchanges.splice(i,1);
              return this.sendExchage.emit(this.exchanges);
            }
          }
      break;
      case 'media':
          for (let i = 0; i < this.multimedias.length; i++) {
            if(this.multimedias[i].link == payload){
               this.multimedias.splice(i,1);
               return  this.sendMultimedia.emit(this.multimedias);
            }
          }
      break;
      case 'noticia':
          this.feedService.deleteNew(payload)
            .subscribe(res => {
               this.globalService.openSnackBar('Noticia ELIMINADA con exito', ':D');
            })

          for (let i = 0; i < this.news.length; i++) {
            if(this.news[i].id == payload){
              return this.news.splice(i,1);
            }
          }
      break;
    }
  }

  abrirAnadidor(){
    this.open = 0;
    this.verlo = true;

  }
  anadir(){
      this.open = -100;
      this.verlo = false;
  }
  cancelar(){
    this.open = -100;
    this.verlo = false;
  }

  noticia(){
    this.seccion = 'a Noticia';
    this.traction = 0;
    this.visualizar = 'noticia';
  }

  rrss(){
    this.seccion = 'a Red Social';
    this.visualizar = 'rrss';

      switch (this.formsource) {
        case 'created':
          this.traction = 0;
          break;
        case 'edited':
          this.traction = 20;
          break;
      }
  }



  exchange(){
    this.seccion = 'o Exchange';
    this.visualizar = 'exchange';
    switch (this.formsource) {
      case 'created':
        this.traction = 33;
        break;
      case 'edited':
        this.traction = 40;
        break;
    }
  }

  multimedia(){
    this.seccion = 'o';
    this.visualizar = 'multimedia';

    switch (this.formsource) {
      case 'created':
        this.traction = 66;
        break;
      case 'edited':
        this.traction = 60;
        break;
    }
  }

  pool(){
    // if(this.piscina == true){
    //
    // }
    this.seccion = 'a Piscina';
    this.traction = 80;
    return  this.visualizar = 'piscina';
  }

  adExchange(){

  }

  ngOnInit(){
    // console.log('DESDE ADCOMPONENT!!!!!!!!!!!!!!!!!!!!');
    // console.log(this.projects);
    this.multimedias = this.projects.multimedia;
    this.exchanges = this.projects.exchanges;
    this.redeSociales = this.projects.socialNetworks;
    // console.log(this.projects);
    this.news = this.projects.news;
    if(this.projects.pool){
    this.piscinActual = this.projects.pool;
    }

    // news:Update[] = [];





    // this.globalService.projectEmitted.subscribe(
    //   response => {
    //
    //     console.log('RECIBIENDO PROJECTO EMITIDO A ADCOMPONENTE...............');
    //     this.projects = response;
    //     console.log(this.projects);
    //     this.multimedias = this.projects.multimedia;
    //     this.exchanges = this.projects.exchanges;
    //     this.redeSociales = this.projects.socialNetworks;
    //     console.log('EXCHANGES............................');
    //     console.log(this.exchanges);
    //     console.log('REDES SOCIALES............................');
    //     console.log(this.redeSociales);
    //     console.log('MULTIMEDIAS............................');
    //     console.log(this.multimedias);
    //   }
    // );

    if(this.formsource == 'created'){
      this.seccion = 'a Red social';
      this.visualizar = 'rrss';
    }
    this.seccion = 'a Noticia';
    this.visualizar = 'noticia';


    if(this.formsource == 'created'){
      return this.itemActiveAncho = 33;
    }
    this.itemActiveAncho = 20;




  }


}
