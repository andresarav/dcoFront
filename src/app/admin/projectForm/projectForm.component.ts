import { Component, ViewChild, OnInit, Input,} from '@angular/core';
import { NgForm } from '@angular/forms';
import {FeedService} from '../../feed/feed.service';
import {GlobalService} from '../../global.service';
import { Ad } from "../../publicaciones/lista.model";
import { ActivatedRoute, Router } from '@angular/router';
import { Project, proyectico } from "../../proyecto/detalle-proyecto/proyecto.model";
import { Coin, Multimedia, Pool, Exchange } from "../../models/modelos.model";
declare var jQuery:any;
declare var $:any;



 @Component({
   selector: "projectForm",
   templateUrl:"./projectForm.component.html",
   styleUrls:["./projectForm.component.css"],
 })

export class ProjectFormComponent implements OnInit {
  constructor(
    private feedService: FeedService,
    private globalService: GlobalService,
    private router: Router,
  ){}

// @Input() project: Project;
stars:number = 1;
bloqtoken:number = 0;
exchanges:Exchange[];
newProject:Project = new Project();
startDate = new Date();
crearVisible:number = 0;

  crear(){
    this.crearVisible += 2;
    // console.log(this.crearVisible);
  }

  sendExchange(mensaje){
    this.newProject.exchanges = mensaje;

  }
  sendSocialNetwork(mensaje){
    this.newProject.socialNetworks = mensaje;

  }
  sendMultimedia(mensaje){
    this.newProject.multimedia = mensaje;
  }


  adExchange(){
  }

  onSubmit(form:NgForm){



    this.newProject.title = form.value.title;
    this.newProject.stars = this.stars;
    this.newProject.type = form.value.type;
    this.newProject.target =  form.value.target;
    this.newProject.bono =  form.value.bono;
    this.newProject.webSite =  form.value.webSite;
    this.newProject.minEth =  form.value.minEth;
    this.newProject.maxEth =  form.value.maxEth;
    this.newProject.whitePaper =  form.value.whitePaper;
    this.newProject.contractAdress =  form.value.contractAdress;
    this.newProject.startDate =  form.value.startDate;
    this.newProject.endDate =  form.value.endDate;
    this.newProject.description =  form.value.description;

    //modelo coin
    this.newProject.coinName =  form.value.coinName;
    this.newProject.simbol =  form.value.simbolCoin;
    this.newProject.usdPrice =  form.value.usdPrice;
    this.newProject.ethPrice =  form.value.ethPrice;
    this.newProject.amountPerEth =  form.value.amountPerEth;
    this.newProject.hardCap =  form.value.hardCap;
    this.newProject.totalSupply =  form.value.totalSupply;
    this.newProject.tokenBloq =  form.value.tokenBloq;
    this.newProject.bloqMessage =  form.value.bloqMessage;
    this.newProject.image =  form.value.imgCoin;

    this.newProject.coinmarketcapId =  form.value.coinmarketcapId;


    // console.log('nuevo proyecto a enviar por el metodo post');
    // console.log(this.newProject);

    // this.visualizarHijo = '';

    if(!form.value.coinmarketcapId){
      this.newProject.coinmarketcapId = 0;
    }
    if(!form.value.target){
      this.newProject.target = 'public';
    }
    if(!form.value.title){
      this.newProject.title = form.value.coinName;
    }
    if(!form.value.imgCoin){
      this.newProject.image = "";
    }

    if(!form.value.bono){
        this.newProject.bono = 0;
    }

    if(!form.value.webSite){
        this.newProject.webSite = 'www';
    }

    if(!form.value.minEth){
        this.newProject.minEth = 0;
    }
    if(!form.value.maxEth){
        this.newProject.maxEth = 0;
    }
    if(!form.value.whitePaper){
        this.newProject.whitePaper = 'www';
    }
    if(!form.value.contractAdress){
        this.newProject.contractAdress = 'www';
    }
    if(!form.value.startDate){
        this.newProject.startDate = new Date();
    }
    if(!form.value.endDate){
        this.newProject.endDate = new Date();
    }
    if(!form.value.description){
        this.newProject.description = 'Proximamente habrá descripción';
    }
    if(!form.value.usdPrice){
        this.newProject.usdPrice = 0;
    }
    if(!form.value.ethPrice){
        this.newProject.ethPrice = 0;
    }
    if(!form.value.amountPerEth){
        this.newProject.amountPerEth = 0;
    }
    if(!form.value.hardCap){
        this.newProject.hardCap = 0;
    }
    if(!form.value.totalSupply){
        this.newProject.totalSupply = 0;
    }
    if(!form.value.tokenBloq){
        this.newProject.tokenBloq = 0;
    }
    if(!form.value.bloqMessage){
        this.newProject.bloqMessage = 'Mensaje de tokens bloqueados';
    }

    if(!form.value.type){
        this.newProject.type = 'ico';
    }


    this.feedService.addProject(this.newProject)
    .subscribe(
      (res)=> {
        this.router.navigate(['/projects', res.id]);
      },
      error => this.globalService.openSnackBar('Al parecer faltan campos por llenar campeón','Error')
    );
  }

  ngOnInit(){
    console.log(this.newProject);
    this.newProject.coinmarketcapId = 0;
    // $('body').css('overflow-y','visible');
  }

bloqstok(){
  this.bloqtoken = 1;
}


starOn(cant){
// alert(cant);
this.stars = cant;
};



}
