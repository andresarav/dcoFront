import { BrowserModule} from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { MomentModule } from 'angular2-moment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';

//Material design imports
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { materialModule } from './material.module';
import 'hammerjs';

// import { InfiniteScrollModule } from 'ngx-infinite-scroll';


//componentes creados por equipo de desarrollo
import { FeedComponents } from './feed/feed-component';
import { FaqComponent } from './faq/faq-component';
import { steemitComponent } from './panelFix/steemit-component';
import { ListComponent } from './publicaciones/lista-component';
import { DetailsComponent } from './proyecto/detalle-proyecto/detalle-proyecto.component';
import { NewsComponent } from './admin/news/news-proyecto.component';
import { ModalComponent } from './emergente/popUpOnboarding/modal.component';
import { ModalHoverComponent } from './emergente/popupHover/modalH.component';
import { ModalHoverFeedComponent } from './emergente/popupHoverFeed/modalH.component';
import { AuthComponent } from './auth/auth-component';
import { ListProjectComponent } from './proyecto/lista-proyecto/lista-proyecto.component';
import { StarComponent } from './proyecto/stars/star.component';
import { SlidersComponent } from './otherComponents/imgSlider/slider-component';
import { SearchtProjectComponent } from './proyecto/busqueda-proyecto/search-proyecto.component';
import { subMenuComponent } from './emergente/subMenu/subMenu.component';
import { AdminComponent } from './admin/adminTemplate.component';
import { adFormComponent } from './admin/adForm/adForm.component';
import { ProjectFormComponent } from './admin/projectForm/projectForm.component';
import { AddFormComponent } from './admin/projectForm/addComponent/addComponent.component';
import { PanelAdminComponent } from './admin/panelAdmin/panelAdmin.component';
import { EditComponent } from './admin/projectForm/editComponent/editComponent.component';
import { popupFilterListComponent } from './emergente/popupFilterList/popupFilterList.component';
import { adverListComponent, DialogAdList } from './admin/lists/adList/adList.component';
import { PanelAdsComponent } from './admin/panelAdmin/panelAds/panelAds.component';
import { userTemplateComponent } from './admin/initi/user.component';
import { projectsTemplateComponent } from './admin/initi/projects.component';
import { UserListComponent, dialogUserDown, dialogUserDelete } from './admin/lists/users/userList.component';
import { PanelUsersComponent } from './admin/panelAdmin/panelUsers/panelUsers.component';
import { AddUserFormComponent } from './admin/userForm/addComponent/addUserComponent.component';
import { popupUserFilterListComponent } from './emergente/popupUserFilterList/popupUserFilterList.component';
import { ModalProfileComponent } from './emergente/popUserProfile/modalProfile.component';




import { SharedModule } from './pipes/shared.module';


import { ProjectListComponent, DialogContentExampleDialog } from './admin/lists/projectList/projectList.component';

import {AuthService}  from './auth/auth.service';
import {GlobalService}  from './global.service';
import { FeedService } from './feed/feed.service';


// import { MatNativeDateModule } from 'angular/material';

import { Routing } from './app.routing';

// Editor de texto textArea
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';


@NgModule({
  declarations: [
    AppComponent,
    FeedComponents,
    FaqComponent,
    steemitComponent,
    ListComponent,
    DetailsComponent,
    NewsComponent,
    ModalComponent,
    ModalHoverComponent,
    ModalHoverFeedComponent,
    AuthComponent,
    ListProjectComponent,
    StarComponent,
    SlidersComponent,
    SearchtProjectComponent,
    subMenuComponent,
    ProjectListComponent,
    AdminComponent,
    adFormComponent,
    ProjectFormComponent,
    AddFormComponent,
    PanelAdminComponent,
    EditComponent,
    popupFilterListComponent,
    adverListComponent,
    PanelAdsComponent,
    userTemplateComponent,
    projectsTemplateComponent,
    UserListComponent,
    PanelUsersComponent,
    AddUserFormComponent,
    popupUserFilterListComponent,
    ModalProfileComponent,
    DialogContentExampleDialog,
    DialogAdList,
    dialogUserDown,
    dialogUserDelete
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    materialModule,
    Routing,
    MomentModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    SharedModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
  ],
  entryComponents: [dialogUserDown, DialogContentExampleDialog, DialogAdList, dialogUserDelete],


  providers: [AuthService, GlobalService, FeedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
