import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { environment } from '../../environments/environment';
import { Project } from '../proyecto/detalle-proyecto/proyecto.model';
import urljoin = require('url-join');
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
declare var jQuery:any;
declare var $:any;
import qs = require ('querystringify');


@Injectable()
export class FeedService {


    private projectsUrl:string;
    private navigate:string;
    private usersUrl:string;
    private urlAds:string;
    private urlApiCMC:string;


    constructor(private http: Http){
      // constructor(){
      this.projectsUrl = urljoin(environment.apiUrl, 'projects');
      this.usersUrl = urljoin(environment.apiUrl, 'me');
      this.urlAds = urljoin(environment.apiUrl, 'notices');
      this.navigate = environment.apiUrl;
      this.urlApiCMC = 'https://api.coinmarketcap.com/v2/ticker/';
    }


    getToken(){
      const token = sessionStorage.getItem('token');
      return token;
    }

    acortaNum(payload){
      const formatoNum = this.number_format(payload)
      const cadena = String(formatoNum);
      return cadena.substr(0,3);
    }

    searchProject(param){
      return `?query=${param}`;
    }


    // API CMC -----------------------------------------------------------------------------------------------------------------------------------------------
    getProjectDetailCmc(coinId:string): Promise<void | any>{

    // const headers = new Headers({ 'Authorization' : this.getToken()});
      const headers = new Headers({ 'content-type': 'text/plain' });
      var url = `${this.urlApiCMC}${coinId}/?convert=ETH`;

      console.log('url:', url);

      return this.http.get(url, {headers: headers})
                  .toPromise()
                  .then(response => {
                    return response.json()
                    // console.log('respuesta: ', response);
                  })
                  .catch(this.handleError);

    }




    // -----------------------------------------------------------------------------------------------------------------------------------------------


    getProjectsVIP(set, filter): Promise<void | Project[]>{

    const headers = new Headers({ 'Authorization' : this.getToken()});
    const url = urljoin(this.usersUrl, 'home', 'projects')

//armar parametrod a enviar
  console.log('dataFILTER', filter);

    const queryParamsObject = {
      limit: 5,
      skip: set,
      query: '',
      where: JSON.stringify({
        type: filter
      })
    };

    console.log('dataFILTERaPPLY', queryParamsObject);


    const queryParams = qs.stringify(queryParamsObject);

    return this.http.get(`${url}?${queryParams}`, {headers:headers})
                .toPromise()
                .then(response => response.json() as Project[])
                .catch(this.handleError);
  }


  getSearchProject(query): Promise<void | any[]>{

  const headers = new Headers({ 'Authorization' : this.getToken()});
  const url = urljoin(this.usersUrl, 'home', 'projects')
  return this.http.get(url + query, {headers:headers})
              .toPromise()
              .then(response => response.json())
              .catch(this.handleError);
  }

  getProjectList(set): Promise<void | any[]>{
  // const token = {'Authorization': this.getToken()}
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const url = urljoin(this.usersUrl, 'home');

  // return this.http.get(url + '?skip=5&limit=5', {headers:headers})
  return this.http.get(`${url}?skip=${set}&limit=5`, {headers:headers})
              .toPromise()
              .then(response => response.json())
              .catch(this.handleError);
  }


  getInfoProject(id): Promise<any | any[]>{
  // const token = {'Authorization': this.getToken()}
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const url = urljoin(this.usersUrl, 'projects', id, 'info');

  return this.http.get(url, {headers:headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
  }



  getProject(id): Promise<void | any>{
  // getProject(id){
    const url = urljoin(this.usersUrl,'projects', id);
    const headers = new Headers({ 'Authorization' : this.getToken()});

    return this.http.get(url, {headers:headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
  }

  // CRUD USUARIOS -----------------------------------------------------------------------------------------------------------------------------------------------


  getUserList(): Promise<void | any[]>{
  // const token = {'Authorization': this.getToken()}
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const url = urljoin(environment.apiUrl, 'users');

  return this.http.get(`${url}?skip=0&limit=100`, {headers:headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
  }


  getUser(): Promise<void | any>{

  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const headers = new Headers({ 'Authorization' : this.getToken()});
  const url = urljoin(this.usersUrl, 'profile');

  return this.http.get(url, {headers:headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
  }



  // EDITAR USUARIO

  editUser(user){
  // const headers = new Headers({ 'Content-Type': 'application/json' });
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = JSON.stringify(user);
  const url = urljoin(environment.apiUrl, 'users', user.id)

  return this.http.put(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  // ELIMINAR USUARIOS
  deleteUser(id_user){
  const headers = new Headers({'Authorization' : this.getToken()});
  const url = urljoin(environment.apiUrl, 'users', id_user);

  return this.http.delete(url, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));

}



  // editProject(cuerpo){
  //   const url = urljoin(this.projectsUrl, cuerpo.id)
  //   const headers = new Headers({ 'Content-Type': 'application/json' });
  //   // const headers = new Headers({ 'Authorization' : this.getToken()});
  //   const body = JSON.stringify(cuerpo);
  //   console.log('este es el cuerpo chamo ----------------');
  //   console.log(body);
  //   console.log(url);
  //
  //   return this.http.put(url, body, {headers:headers})
  //       .map((resul:Response) =>{
  //          const data =  resul.json();
  //          return data;
  //        })
  //       .catch((error: Response) => Observable.throw(error.json()));
  //
  // }






  // CAMBIAR ROL DE USUARIO---------------------------------------------------------------------------------------------------------------------------------------------

  roleChangeVip(userId){
  const headers = new Headers({'Authorization': this.getToken()});
  const body = {};
  const url = urljoin(environment.apiUrl, 'admin', 'users', userId, 'rolevip');

  return this.http.post(url, body, {headers:headers})
        .map((resul:Response) => {
          const data = resul.json();
          return data;
        })
        .catch((error:Response) => Observable.throw(error.json()));
  }



  // CRUD WALLETS -----------------------------------------------------------------------------------------------------------------------------------------------

  addWallet(cuerpo){
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(cuerpo);
  const body = cuerpo;
  const url = urljoin(this.usersUrl, 'wallets')

  return this.http.post(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }


  deleteWallet(id_wallet){
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = JSON.stringify(id_wallet);
  const url = urljoin(this.usersUrl, 'wallets', id_wallet)
  // console.log(url);
  // console.log(id_wallet);
  return this.http.delete(url, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }










  // CRUD PAYMENTS -----------------------------------------------------------------------------------------------------------------------------------------------

  // /users/:id_user/payments/admin

  addPaymentByAdmin(payment, id_user){
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(payment);
  const body = payment;
  const url = urljoin(environment.apiUrl, 'admin', 'users', id_user, 'payments', 'admin')

  return this.http.post(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  addPayment(payment){
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(payment);
  const body = payment;
  const url = urljoin(this.usersUrl, 'payments')

  return this.http.post(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }


  deletePayment(id_payment){
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(payment);
  // const body = payment;
  const url = urljoin(this.usersUrl, 'payments',id_payment)

  return this.http.delete(url, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }


  deletePaymentByAdmin(id_payment){
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(payment);
  // const body = payment;
  const url = urljoin(environment.apiUrl, 'admin', 'payments', id_payment, 'admin')

  return this.http.delete(url, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }


  verifyPayment(id_payment, id_user, endDate){
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(payment);
  // const body = endDate;
  const body = ({endDate:endDate});

  const url = urljoin(environment.apiUrl, 'admin', 'users', id_user, 'payments', id_payment, 'verify')

  return this.http.post(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }


  // MEMBRESÍA

  verifyMembership(id_user){

  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = ({endDate:endDate});
  const body = {};

  const url = urljoin(environment.apiUrl, 'admin', 'users', id_user, 'verify')

  return this.http.post(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }













  //CRUD ANUNCIOS ------------------------ -----------------------------------------------------------------------------------------------------------------

  addNotice(cuerpo){
    // const url = urljoin(environment.apiUrl, 'notices')
    const headers = new Headers({ 'Authorization' : this.getToken()});
    // const headers = new Headers({ 'Content-Type': 'application/json' });
    // const body = JSON.stringify(cuerpo);
    const body = cuerpo;

    return this.http.post(this.urlAds, body, {headers:headers})
        .map((resul:Response) =>{
           const data =  resul.json();
           return data;
         })
        .catch((error: Response) => Observable.throw(error.json()));
  }

  getNotices(): Promise<void | any[]>{

  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const url = urljoin(this.urlAds, 'home');

  return this.http.get(this.urlAds, {headers:headers})
              .toPromise()
              .then(response => response.json())
              .catch(this.handleError);
  }

  deleteNotice(id){
  const url = urljoin(this.urlAds, id)
  // const headers = new Headers({ 'Content-Type': 'application/json' });
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = {};
  return this.http.delete(url, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  updateNotice(cuerpo){

  const url = urljoin(this.urlAds, cuerpo.id)
  // const headers = new Headers({ 'Content-Type': 'application/json' });
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = JSON.stringify(cuerpo);

  // const body = {};
  return this.http.put(url,body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }








  // SERVICIOS ADMINISTRADOR, CRUD PROYECTO ------------------------------------------------------------------------------------------------------------

  getProjectsADMIN(): Promise<void | any[]>{

  const headers = new Headers({ 'Authorization' : this.getToken()});
  return this.http.get(this.projectsUrl, {headers:headers})
            .toPromise()
            .then(response => response.json() as Project[])
            .catch(this.handleError);
  }



  addProject(body){
  // const url = urljoin(environment.apiUrl, 'notices')
  // const headers = new Headers({ 'Content-Type': 'application/json' });
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(cuerpo);

  return this.http.post(this.projectsUrl, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }


  editProject(cuerpo){
  const url = urljoin(this.projectsUrl, cuerpo.id)
  // const headers = new Headers({ 'Content-Type': 'application/json' });
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(cuerpo);
  const body = cuerpo;

  return this.http.put(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }







  addNew(cuerpo){
  const url = urljoin(environment.apiUrl, 'news')
  // const headers = new Headers({ 'Content-Type': 'application/json' });
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = JSON.stringify(cuerpo);
  const body = cuerpo;

  return this.http.post(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         console.log('noticia');
         console.log(data);
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  deleteNew(id){
  const url = urljoin(environment.apiUrl, 'news', id)
  // const headers = new Headers({ 'Content-Type': 'application/json' });
  const headers = new Headers({ 'Authorization' : this.getToken()});
  // const body = {};
  return this.http.delete(url, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }



  addPool(cuerpo){
  const url = urljoin(environment.apiUrl, 'pools')
  const headers = new Headers({ 'Content-Type': 'application/json' });
  // const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = JSON.stringify(cuerpo);
  // const body = cuerpo;

  return this.http.post(url, body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }



  borrarADMIN(id){
  const url = urljoin(this.projectsUrl, id)
  const headers = new Headers({ 'Authorization' : this.getToken()});

  return this.http.delete(url, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }













  //SUSCRIBIR y DESUSCRIBIR----------------------------------------------------------------------------

  getSuscriptions():Promise<void | any>{
    const headers = new Headers({ 'Authorization' : this.getToken()});
    const url = urljoin(this.usersUrl, 'subscriptions');
    return this.http.get(url,{headers:headers})
                    .toPromise()
                    .then(response => response.json())
                    .catch(this.handleError);
  }


  // getSearchProject(query): Promise<void | any[]>{
  //
  // const headers = new Headers({ 'Authorization' : this.getToken()});
  // const url = urljoin(this.usersUrl, 'home', 'projects')
  // return this.http.get(url + query, {headers:headers})
  //             .toPromise()
  //             .then(response => response.json())
  //             .catch(this.handleError);
  // }

  suscribir(id){
  const url = urljoin(this.usersUrl, 'projects', id, 'subscribe')
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = {};
  return this.http.post(url,body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  deSuscribir(id){
  const url = urljoin(this.usersUrl, 'projects', id, 'subscribe')
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = {};
  return this.http.delete(url, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }




  //SERVICIOS REACTIONS LIKE-DISLIKE-----------------------------------------------------------------------------------------------------------

  likeProject(id){
  const url = urljoin(this.usersUrl, 'projects', id, 'like')
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = {};
  return this.http.post(url,body, {headers:headers})
      .map((resul:Response) =>{
         const data =  resul.json();
         return data;
       })
      .catch((error: Response) => Observable.throw(error.json()));
  }


  dislikeProject(id){
  const url = urljoin(this.usersUrl, 'projects', id, 'dislike')
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = {};
  return this.http.post(url,body, {headers:headers})
    .map((resul:Response) =>{
       const data =  resul.json();
       return data;
     })
    .catch((error: Response) => Observable.throw(error.json()));
  }


  deleteReaction(id){

  const url = urljoin(this.usersUrl, 'projects', id, 'like-dislike')
  const headers = new Headers({ 'Authorization' : this.getToken()});
  const body = {};
  return this.http.delete(url, {headers:headers})
    .map((resul:Response) =>{
      console.log(resul);
       // const data =  resul.json();
       // return data;
     })
    .catch((error: Response) => Observable.throw(error.json()));
  }

  // errores -----------------------------------------------------------------

  handleError(error: any){
      const errMsg = error.message ? error.message :
      error.status ? `${error.status} - ${error.statusText}`: 'Server Error';
      return console.log(errMsg);
  }
















  countDownD(termina: Date){

    // // var countDownDate = new Date(this.project.pool.endDate.getFullYear(),this.project.pool.endDate.getMonth(),this.project.pool.endDate.getDate()).getTime();
    // var countDownDate = new Date(termina.getFullYear(),termina.getMonth(),termina.getDate()).getTime();
    //
    // // Update the count down every 1 second
    // const x = setInterval(function() {
    //
    //
    //     // Get todays date and time
    //     var now = new Date().getTime();
    //
    //     // Find the distance between now an the count down date
    //     var distance = countDownDate - now;
    //
    //     // Time calculations for days, hours, minutes and seconds
    //     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    //     var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    //     var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    //     var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    //
    //     // Output the result in an element with id="demo"
    //     document.getElementById("demo").innerHTML = days + " : " + hours + " : "
    //     + minutes + " : " + seconds + " ";
    //
    //     // If the count down is over, write some text
    //     if (distance < 0) {
    //         clearInterval(x);
    //         document.getElementById("demo").innerHTML = "FINALIZADO";
    //     }
    // }, 1000);
    //

  }


  number_format(amount) {

      amount += ''; // por si pasan un numero en vez de un string
      amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

      amount = '' + amount.toFixed(0);

      var amount_parts = amount.split('.'),
          regexp = /(\d+)(\d{3})/;

      while (regexp.test(amount_parts[0]))
          amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

      return amount_parts.join('.');
  }


      abrirAlterno(payload){
        $('.alterno').addClass('abrirAlterno');
        sessionStorage.setItem("alterno", payload)
        console.log(payload);
      }

      toggleAlterno(){
        $('.subAlterno').toggleClass('abrirSubAlterno');
      }

      cerrarAlterno(payload){
        $('.alterno').removeClass('abrirAlterno');
        sessionStorage.setItem("alterno", payload)
        console.log(payload);
      }

      escondeScroll(payload){
        // if(payload== true){
        //   $('body').css('overflow-y','hidden');
          $('body, html').animate({
            scrollTop: 0
          }, .2);
        // }else{
        //   $('body').css('overflow-y','visible');
        // }
      }

      FuncionScrollDetail(payload){

            if(payload === true){
              //SI ESTOY DENTRO DE DETALLE PROYECTO APLIQUE LOS SIGUIENTES ESTILOS
                $(window).on("scroll",function(){
                if($(window).scrollTop()>398){
                  $(".mat-toolbar").css("top","-55px");
                } else{
                  $(".mat-toolbar").css("top","0px");
                }
                });

                $(window).on("scroll",function(){
                if($(window).scrollTop()>450){
                  $(".barritaPanel").css("height","50px");
                  $("#menuSec").css("height","50px");
                  $("#menuSec").css("box-shadow","0px 3px 5px 0px rgba(0,0,0,0.22)");
                  $("#panelFix").css("position","fixed");
                  $("#panelFix").css("height","100%");
                  $(".infoDes").css("top","0px");
                  $("#menuSec").css("position","fixed");
                  $("#detalleUser").css("top","0px");
                  $(".infoUser").css("height","100%");
                  $(".subAlterno").css("display","initial");

                } else{
                  $(".subAlterno").css("display","none");
                // $(".infoUser").css("height","380px");
                $("#detalleUser").css("top","55px");
                $("#panelFix").css("height","590px");
                $(".infoDes").css("top","80px");
                $(".barritaPanel").css("height","55px");
                $("#menuSec").css("box-shadow","0px 3px 5px 0px rgba(0,0,0,0.01)");
                $("#menuSec").css("position","absolute");
                $("#menuSec").css("height","55px");
                $("#panelFix").css("position","absolute");

                }
              });
              return console.log('');
            }

            //SI ESTOY DENTRO DE FEED O PANTANLLA INICIAL APLICAR LOS SIGUIENTES ESTILOS

            $(window).on("scroll",function(){
            if($(window).scrollTop()>398){
              $(".mat-toolbar").css("top","0px");
              $(".subAlterno").css("display","initial");
              $("#detalleUser").css("top","45px");
              // $(".infoUser").css("height","380px");

            } else{
              $(".subAlterno").css("display","initial");
              $(".mat-toolbar").css("top","0px");
              $("#detalleUser").css("top","45px");
              // $(".infoUser").css("height","380px");
            }
            });
            return console.log('');

      }

      mostrarModal(){
        // $('#contenedorModal').css('display','initial');
        $('#modal').css('animation','animationIn .8s forwards');
        $('#overlay').addClass('active');

      };


      progresadoBar(anchito){

        var carinito;

        if(anchito <= 70){
          return carinito = 'linear-gradient(to bottom right, #09E9A3, #338c99)';
        }else if(anchito <= 80){
          return carinito = 'linear-gradient(to bottom right, #09E9A3, yellow)';
        }else if(anchito <= 90){
          return carinito = 'linear-gradient(to bottom right, yellow, #ff6300)';
        }else if(anchito <= 100){
          return carinito = '#8a29b3';
        }
      }



}
