import { Component, ViewChild, OnInit, Input} from '@angular/core';
import { NgForm } from "@angular/forms";
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';
import  {FeedService}  from './feed.service';
import  {GlobalService}  from '../global.service';
import  {AuthService} from '../auth/auth.service';
import { ProjectList, CoinData, Ad, ModalResponse } from '../publicaciones/lista.model';
import { Multimedia } from '../models/modelos.model';


// import { MatTableDataSource, MatSort } from '@angular/material';
// import { User } from '../user.model';
// declare var jQuery:any;
// declare var $:any;


 @Component({
   selector: "app-feed",
   templateUrl:"./feed.component.html",
   styleUrls:["./feed.component.css"],
 })

export class FeedComponents implements OnInit {

      constructor(public sanitizer:DomSanitizer,
                  public globalService:GlobalService,
                  public authService:AuthService,
                  public feedService:FeedService,
      ){}


      @Input() url: SafeResourceUrl;
      status:string;
      id:string = "http://www.youtube.com/embed/cUbRC6dzCLs";
      statusModal:string;
      contenidoModal:string;
      alimentadorFeed:any[];
      anchoBusqueda:number = 33;
      busqueda:boolean;
      beforeTraction:number;
      queryParam:any;
      alternoResponsive:string;
      dataFil:string[]=['ico','market','presale'];

selected:any = 'todo';

async searchProject (form: NgForm){

  this.visualizar = 'nada';

  this.setProjects = 0;
this.moreProject = false;
this.onlyFilter = false;



  this.queryParam = await this.feedService.searchProject(form.value.search);

  setTimeout(()=>{  this.visualizar = 'busqueda';}, 100);

}

applyFilter(payload:string){


  console.log('FILTRO DE CONTENIDO: ', payload);
  var dataFil = [payload];

  if(payload == 'todo'){
    dataFil=['presale','ico','market'];
  }



  this.visualizar = 'nada';

  this.setProjects = 0;
  this.moreProject = false;
  this.dataFil = dataFil;
  // this.onlyFilter = false;

  // this.queryParam = await this.feedService.searchProject(form.value.search);

  setTimeout(()=>{  this.visualizar = 'projects';}, 100);

}

onScroll(){
  console.log('scrolleado');
}

reciboStatusModal(dato:string){
  return  this.statusModal = dato;
}

cerrarModal(payload){
  return this.contenidoModal = payload;
}

reciboContentModal(content:string){
  // console.log(`recibi ${content} como parametro`);
  return  this.contenidoModal = content;
}

buscar(payload){
  // alert(payload);
  // this.onlyFilter = false;

  if(payload == 'yes'){

    this.busqueda = true;
    this.traction = 0;
   return  this.anchoBusqueda = 100;
  }
  this.busqueda = false;
  this.traction = this.beforeTraction;
  return  this.anchoBusqueda = 33;
}



loading:boolean = true;
traction:number = 0;
visualizar:string;
onlyFilter:boolean = false;
// alimentadorFeed:any[];

todoya?:boolean = false;

todo(){
  this.setProjects = 0;
  this.moreProject = false;
  this.onlyFilter = false;
  this.traction = 0;
  this.beforeTraction = this.traction;
return  this.visualizar = 'all';

}
moreProject:boolean = false;
proyectos(){
  this.setProjects = 0;
  this.moreProject = false;
  this.onlyFilter = true;
   this.traction = 33;
   this.beforeTraction = this.traction;

  return this.visualizar = 'projects';
}

calendarVip(){
  this.onlyFilter = false;
   this.traction = 66;
   this.beforeTraction = this.traction;
  return this.visualizar = 'calendar';

}


hoy:Date = new Date;
item:any;
diasTotal:any;
diasTranscu:any;
barDiasProgress:any;

// Iniciar Onboarding --------------------------------------------------------------------
async iniciarTutorial(){
  const visitas = parseInt(localStorage.getItem('visitas'));
  var verifyVisit = localStorage.getItem('verifyVisit');
  if(visitas<2 && verifyVisit=='true'){
    this.contenidoModal='newUser';
    console.log('-----------------------------------------------------------------------------------');
    console.log(verifyVisit);
    console.log('-----------------------------------------------------------------------------------');
    verifyVisit = 'false';
    localStorage.setItem('verifyVisit', JSON.stringify(verifyVisit));
    return this.statusModal = 'active';
  }
}

setProjects:number = 0; //Variable para manejar el index de los proyectos que deben de ser cargados
loadlist:boolean = false;

cargarMas(visualizar){
  console.log(visualizar);
  this.setProjects+=5;
  this.loadlist = true;

  if(this.visualizar == 'projects'){

    this.feedService
    .getProjectsVIP(this.setProjects, this.dataFil)
    .then((projects:any[])=> {
      this.globalService.sendlistProject(projects, 'projects');
      console.log('Estos son los proyectos: ', projects);
      this.loadlist = false;
    });
  }
  if(this.visualizar == 'all'){
    this.feedService
    .getProjectList(this.setProjects)
    .then((projects:any[])=> {

      // this.projects = projects;
      // this.loading = false;
      this.globalService.sendlistProject(projects, 'all');
      this.loadlist = false;
    });
  }

}

reciboStatus(status){
  this.moreProject = status;
}


role:any;
// role:boolean;

  ngOnInit(){

    this.iniciarTutorial();
    this.todo();
    this.role = "user";
    if(this.authService.isLoggedIn()){
      this.role = this.authService.getRoleStorage();
    }
    // this.role = this.feedService.getRoleStorage();
    this.globalService.sendLoading();
    this.globalService.panelAdminVisible('no es visible', 'feed');
    this.feedService.FuncionScrollDetail(false);
    // alternoResponsive:string;

    this.globalService.alternoEmitted.subscribe(res => {
      switch (res) {
        case 'abrir':
          this.alternoResponsive = 'abrir';
          break;
          case 'cerrar':
            this.alternoResponsive = 'cerrar';
            break;
      }
    });




  }


}
