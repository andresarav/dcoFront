import { Component, OnInit } from '@angular/core';
import {AuthService} from './auth/auth.service';
import {FeedService} from './feed/feed.service';
import {GlobalService} from './global.service';
import { User } from './models/modelos.model';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private feedService: FeedService,
    private globalService: GlobalService,
    // private _helper: HelperService
  ){}




  names:any;
  title = 'app';
  userName:any ;
  roles:any;
  statusModal:any;
  contenidoModal:any;
  selectorSubMenu:string;
  clases:any;
  estadoControlMenu:any;
  burgerState:string = "cerrar";
  role:any;
  visible:any = false;
  picture:string;
  visits:number;
  currentUser:User;

 contadorVisitas = async() => {
   this.visits = await parseInt(localStorage.getItem('visitas'));

  if(isNaN(this.visits)){
    this.visits = 0;
  }
  this.visits++;
  localStorage.setItem('visitas', JSON.stringify(this.visits));
}


loadUserAutomatic(){
  this.feedService.getUser()
      .then( (res) => {

        if(res == undefined){
          return this.globalService.openSnackBar('Hola querido, te invito a iniciar sesión con Discord :)', 'Hi!');
        }


        const { blocked, verifyMembership, wallets, payments, subscriptions, membership,  email, id, username, name, birthday, createdAt, image, role, txAudit } = res;
        const { passedDays, percentagePassed,  totalDays  } = res.extra;


        this.currentUser = new User(
          id,
          birthday,
          name,
          username,
          wallets,
          payments,
          email,
          role,
          // this.authService.getRole(3, id),
          subscriptions,
            blocked,
          txAudit,
          membership,
          null,
          this.authService.parseProfileImage(image),
          createdAt,
          10,
          passedDays,
          percentagePassed,
          totalDays,
          verifyMembership
        )

        sessionStorage.removeItem("user");
        sessionStorage.setItem('user', JSON.stringify(this.currentUser));

      })
}


pruebadeFuego(){
    var numbers = [1, 2, 3, 4, 5 ];

    // console.log('--------------------------------------------------------------');
    // console.log(_.last(numbers, 3));
    // console.log('--------------------------------------------------------------');
}


esconderScrolll(){
  this.feedService.escondeScroll(true);
}


  ngOnInit(){

    // this.pruebadeFuego();
    this.contadorVisitas();

    // var x = setTimeout(()=> {this.loadUserAutomatic();},10000)
    //Cargar datos de usuario 10 segundos despues, ya que los datos se almacenan en localstorage, esto facilitarà actualizar cambios de roles y visualizaciòn para el usuario



    this.globalService.changeEmitted.subscribe(
      response => {
        return this.estadoControlMenu = response;
      }
    );


  }




  reciboAdminTemplate(payload){
    console.log('recibido en menu template padre');
    console.log(payload);

  }


  abrirSub(payload){
    this.statusModal='active';
    this.selectorSubMenu = payload;
    console.log(this.clases);

    if(this.selectorSubMenu == 'adminOp'){
      this.contenidoModal = [

        {nombre:'Proyectos y anuncios',   link:'/admin', icono:'layers', routeLink:'/admin', func:''},
        {nombre:'Usuarios',   link:'/admin/users', icono:'person', routeLink:'/admin/users', func:''},

      ];

      // this.clases = [ { top:45, left:3 } ];
      this.clases = 'user';

    }else if(this.selectorSubMenu == 'adminOp2'){
      this.contenidoModal = [

        {nombre:'Perfil',   link:'/admin', icono:'verified_user', routeLink:'', func:'perfil'},
        {nombre:'Cerrar Sesión',   link:'/admin/users', icono:'keyboard_tab', routeLink:'', func:'logout'},

      ];

      // this.clases = [ { top:45, left:88 } ];
      this.clases = 'opUser';

    }
  }

  isLoggedIn(){
    return this.authService.isLoggedIn();
  }

  logout() {
    return this.authService.logout();
  }

  name(){
    // const user = localStorage.getItem('user');
    const { userName } = JSON.parse(sessionStorage.getItem('user'));
    return this.userName = userName;
  }

  getRole(){
    // const user = localStorage.getItem('user');
    const { role } = JSON.parse(sessionStorage.getItem('user'));
    return this.role = role;
  }

  pictureP(){
    const { perfilPic } = JSON.parse(sessionStorage.getItem('user'));
    return this.picture = perfilPic;
  }


  reciboStatustModal(payload){
    this.statusModal = payload;
  }

  ocultar(){
    this.statusModal = 'desactivado';
    console.log(this.statusModal);
  }



}
