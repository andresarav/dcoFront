import { Component, ViewChild, Input, OnInit, Output, EventEmitter } from '@angular/core';
// import { MatTableDataSource, MatSort } from '@angular/material';
import { Steemit } from './steemit.model';
import {FeedService} from '../feed/feed.service';
import {AuthService} from '../auth/auth.service';
import {GlobalService} from '../global.service';
import { User, Subscription, Pool } from '../models/modelos.model';
declare var jQuery:any;
declare var $:any;



 @Component({
   selector: "app-steemit",
   templateUrl:"./steemit.component.html",
   styleUrls:["./steemit.component.css"],
 })

export class steemitComponent implements OnInit  {

  @Output() statusModal: EventEmitter <string> = new EventEmitter<string>();
  @Output() contenidoModal: EventEmitter <string> = new EventEmitter<string>();

    constructor(
      private feedService: FeedService,
      private authService: AuthService,
      private globalService: GlobalService

    ){}



loading:boolean = true;
screenView:string;
user:any;
role:string = "user";
caducaM:any;
avanceProject:any = 90;
traction:number = 0;
subscris:Subscription[] = [];
// suscriptions:Subscription[] = [];

anchito:number;
carinito:string;


unSubscriProject(idProject){
  return this.feedService.deSuscribir(idProject)
      .subscribe(
      () => {console.log('DESUBSCRIPTION realizada con exito')
      this.loading = true;
      this.feedService
      .getSuscriptions()
      .then((sus)=>{
        console.log('RESULTADO SUSCRIPCIONES')
        console.log(sus);

        if(sus.length > 0){
          this.suscrip();
          this.subscris=sus;
          console.log('array suscripciones', this.subscris)
          return this.loading = false;
        }
        this.subscris.length = 0;
        this.blog();

      });

    },
      error => this.feedService.handleError(error)
    );
}

estalogueado(){
    return this.authService.isLoggedIn();
}

toggleAlterno(){
  this.feedService.toggleAlterno();
}

obtenerSuscriptions(){

}


ngOnInit(){


  this.blog();

  if(this.authService.isLoggedIn()){

    this.user = this.authService.getUserStorage();
    this.caducaM = this.user.totalDays - this.user.passedDays;
    this.anchito = this.user.percentagePassed;
    this.avanceProject = this.user.percentagePassed;
    this.role = this.user.userRole;

    this.feedService
    .getSuscriptions()
    .then((sus)=>{
      console.log('RESULTADO SUSCRIPCIONES')
      console.log(sus);

      if(sus.length > 0){

        this.suscrip();
        this.subscris=sus;
        this.loading = false;

      }
    });

    }
    this.carinito = this.feedService.progresadoBar(this.anchito);
  }


  suscrip(){
    this.traction = 0;
    this.screenView = "suscriptions";
  }

  blog(){
    this.traction = 50;
    this.screenView = "blog";
  }




contentModal(contenido){
  return  this.contenidoModal.emit(contenido);
}

renovarMember(){
  this.globalService.sendUpdateMembership('active', true);
}

enviarStatus(){
return  this.statusModal.emit('active');
}
// panelOpenState: boolean = true;
// steemits:Steemit[] = new Array(10).fill(steemArt); //users es un array del modelo User

  steemits:Steemit[] = [
    new Steemit('Unboxing: Nuevo Trezor Modelo T', new Date, 'https://steemitimages.com/256x512/https://img.youtube.com/vi/snM_YpqsdmY/0.jpg', 'https://steemit.com/spanish/@dineroconopcion/unboxing-nuevo-trezor-modelo-t'),
    new Steemit('Criptomonedas Noticias: Ethereum Classic', new Date, 'https://steemitimages.com/256x512/https://img.youtube.com/vi/R-8uCqV-dBk/0.jpg', 'https://steemit.com/spanish/@dineroconopcion/criptomonedas-noticias-ethereum-classic'),
    new Steemit('Noticias Criptomonedas: Chile', new Date, 'https://steemitimages.com/256x512/https://i.gyazo.com/870f50580f67261a8dfca51307918dab.jpg', 'https://steemit.com/spanish/@dineroconopcion/noticias-criptomonedas-chile'),
    new Steemit('Criptomonedas Tutorial: Como Utilizar IDEX', new Date, 'https://steemitimages.com/256x512/https://i.gyazo.com/22e1d06e9c7acb2d8b3417af79ecb6e8.jpg', 'https://steemit.com/spanish/@dineroconopcion/criptomonedas-tutorial-como-utilizar-idex'),
    new Steemit('Criptomonedas Actualizaciones: Vechain', new Date, 'https://steemitimages.com/256x512/https://i.gyazo.com/b1a042c9304fc11c7241d39628aa7833.jpg', 'https://steemit.com/spanish/@dineroconopcion/criptomonedas-actualizaciones-vechain'),
  ]

nuevoa(){
}

step = 1;

}
