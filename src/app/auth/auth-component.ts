import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User, Subscription, Pool } from '../models/modelos.model';
import {AuthService} from './auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import {FeedService} from '../feed/feed.service';
import { environment } from '../../environments/environment';

// import { MatTableDataSource, MatSort } from '@angular/material';
// import { Faq } from './faq.model';
import {GlobalService} from '../global.service';
declare var jQuery:any;
declare var $:any;


 @Component({
   selector: "app-auth",
   templateUrl:"./auth.component.html",
   styleUrls:["./auth.component.css"],
 })


export class AuthComponent implements OnInit  {
    // constructor(private feedService: FeedService){}

    // @Input() faqP:string;
constructor(private authService: AuthService,
private route: ActivatedRoute,
private globalService: GlobalService,
private router: Router,
private feedService: FeedService,

){}
    signinForm: FormGroup;
    code:any;
    text:string = "ó";

    pool:Pool = new Pool(
      null,
      1,
      2,
      'https://primablock.com/pool/0xaee72ecc0d8bf5e06fbcc6d9ebcb4a4a186bda0a/contributor',
      new Date(),
      true,
      false,
    );

    subscription:Subscription = new Subscription(
      'BoonTech',
      this.pool
    );


    subscriptions:Subscription[] = new Array(10).fill(this.subscription); //users es un array del modelo User


    ngOnInit(){

      this.globalService.panelAdminVisible('no es visible', 'feed');


      this.signinForm = new FormGroup({
        email:new FormControl(null, [
          Validators.required, //primera validación es que sea requerido
          Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ]),
        password: new FormControl(null, Validators.required)
      });



      this.code = this.route.snapshot.queryParams.code;

      if(this.code){
        // alert(`llego el code: ${this.code}`);
        return this.loginPost(this.code);
      }
    }


  loginPost(code){

        this.authService.LoginDiscord(code)
				.subscribe(
					(res)=> {
            this.login();
          },
					error => console.log(error)
				);

    }

currentUser:User;

    login(){
        this.feedService.getUser()
         .then( res => {

           if(res == undefined){
             this.globalService.openSnackBar('Estas bloqueado de forma indefinida querido', 'Error');
             return this.authService.logout();
           }

           const { blocked, verifyMembership, wallets, payments, subscriptions, membership,  email, id, username, name, birthday, createdAt, image, role, txAudit } = res;
           const { passedDays, percentagePassed,  totalDays  } = res.extra;


           this.currentUser = new User(
             id,
             birthday,
             name,
             username,
             wallets,
             payments,
             email,
             role,
             // this.authService.getRole(3, id),
             subscriptions,
             // this.subscriptions,
             blocked,
             txAudit,
             membership,
             null,
             this.authService.parseProfileImage(image),
             createdAt,
             10,
             passedDays,
             percentagePassed,
             totalDays,
             verifyMembership
           )

           sessionStorage.setItem('user', JSON.stringify(this.currentUser));

           var verifyVisit = 'true';
           localStorage.setItem('verifyVisit', verifyVisit);

           this.router.navigate(['/'])

         });
      }



    onSubmit(a){
      if(this.signinForm.valid){

         const { email, password } = this.signinForm.value;
         const user = [{mail:email, pass:password}];
         console.log(user);
      };
    }


    loginDis(){
      // https://dcoweb.app/auth
      // const url = "https://discordapp.com/api/oauth2/authorize?client_id=445639978282909699&redirect_uri=https%3A%2F%2Fdcoweb.app%2Fauth&response_type=code&scope=identify%20guilds%20guilds.join%20email%20connections%20gdm.join";
      // LocalHost
      // const url = "https://discordapp.com/api/oauth2/authorize?client_id=445639978282909699&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2Fauth&response_type=code&scope=identify%20guilds%20email%20guilds.join%20connections%20gdm.join";
      // http://149.28.60.180:4200/auth
      // const url = "https://discordapp.com/api/oauth2/authorize?client_id=445639978282909699&redirect_uri=http%3A%2F%2F149.28.60.180%3A4200%2Fauth&response_type=code&scope=identify%20guilds%20guilds.join%20email%20connections%20gdm.join";
      const url = environment.urlRedirect;


      return window.location.href=url;
      // return this.authService.salude(url);
    }

}
