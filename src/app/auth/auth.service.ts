import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { Project } from '../proyecto/detalle-proyecto/proyecto.model';
import { User } from '../models/modelos.model';
import { Router } from '@angular/router';
import {GlobalService} from '../global.service';
import {FeedService} from '../feed/feed.service';

import urljoin = require('url-join');
import 'rxjs/add/operator/toPromise';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


declare var jQuery:any;
declare var $:any;


@Injectable()
export class AuthService {

    private usersUrl:string;
    private navigate:string;

    public currentUser?:User;

    constructor(private http: Http,
      private router: Router,
      private globalService: GlobalService,
    private feedService: FeedService,

    ){
      // constructor(){
      this.usersUrl = urljoin(environment.apiUrl, 'me');
      this.navigate = environment.apiUrl;

    }


  //   getProjects(): Promise<void | Project[]>{
  // // getProjects(){
  //   console.log(this.projectsUrl);
  //   return this.http.get(this.projectsUrl)
  //               .toPromise()
  //               .then(response => response.json() as Project[])
  //               .catch(this.handleError);
  // }




  LoginDiscord(code){

      const body = {code:code};
      // const headers = new Headers({ Authorization: 'accessToken' });
      const headers = new Headers({ 'Content-Type': 'application/json' });

      // const options = new RequestOptions({ headers : headers, method: 'post'});
      const url = urljoin(this.usersUrl, 'discord', 'login')

      return this.http.post(url, body, {headers:headers})
      .map((resul:Response) =>{

         const json = resul.json();
         this.sendToken(json);
         return json;
       })
      .catch((error: Response) => {
        this.globalService.openSnackBar('No se ha podido AUTENTICAR', 'ERROR')
        return Observable.throw(error.json())
      } );

  }

  parseProfileImage(pic){
  var image = "";

  if(pic.substr(-8) == "null.png"){
    //si el usuario no tiene foto de perfil le asignamos una predeterminada
    return image = "../../assets/pictureProfile.jpg";
  }
  return pic;
  }


  sendToken = (usuario) => {
  const { accessToken } = usuario;
  sessionStorage.setItem('token', accessToken);

  };


  getRole(id, userId){
  let user = "";

    switch(id){
      case 1:
        return  user = "user";
      case 2:
        return  user = "vip";
      case 3:
        return  user = "admin";
    }
  }

  getRoleStorage(){
    const { role } = JSON.parse(sessionStorage.getItem('user'));
     return role;
  }

  getUserStorage(){
    const user  = JSON.parse(sessionStorage.getItem('user'));
     return user;
  }


  isLoggedIn(){
  return sessionStorage.getItem('token') !== null;
  }

  logout(){
  sessionStorage.removeItem("token");
  sessionStorage.removeItem("user");
  // localStorage.clear();
  this.currentUser = null;
  this.router.navigateByUrl('/auth');
  }

  salude(url){
    window.location.href=url;
    // alert(url);
  }

  handleError(error: any){
      const errMsg = error.message ? error.message :
      error.status ? `${error.status} - ${error.statusText}`: 'Server Error';
      return console.log(errMsg);
  }


}
