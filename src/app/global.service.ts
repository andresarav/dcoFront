import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import urljoin = require('url-join');
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import {MatSnackBar} from '@angular/material';


@Injectable()
export class GlobalService {


    constructor(
      public snackBar: MatSnackBar
    ){}

    // Observable string sources
      // Observable string streams
      // Service message commands


      private emitChangeSource = new Subject();
      changeEmitted = this.emitChangeSource.asObservable();
      emitChange(vista: any) {
          this.emitChangeSource.next(vista);
      };


      private panelAdminVisibleSource = new Subject();
      panelAdminEmitted = this.panelAdminVisibleSource.asObservable();
      panelAdminVisible(visible, screen) {
        // console.log(`esta es la respuesta del componente admin ${visible}`);
        const body = [
          visible,
          screen
        ];
          this.panelAdminVisibleSource.next(body);
      };


      private projectSource = new Subject();
      projectEmitted = this.projectSource.asObservable();
      sendProject(project){
        this.projectSource.next(project);
      }


      private loadingSource = new Subject();
      loadingEmitted = this.loadingSource.asObservable();
      sendLoading(){
        this.loadingSource.next();
      }


      private buttonSaveSource = new Subject();
      buttonSaveEmitted = this.buttonSaveSource.asObservable();
      sendButtonSave(estado){
        this.buttonSaveSource.next(estado);
      }



      private loadingSources = new Subject();
      loadingEmitteds = this.loadingSources.asObservable();
      sendLoadings(estado){
        this.loadingSources.next(estado);
      }


      //Servicio para abrir y cerrar el menu alterno en el responsive
      private alternoSources = new Subject();
      alternoEmitted = this.alternoSources.asObservable();
      sendAlternoMenu(estado){
        this.alternoSources.next(estado);
      }

      //Servicio para notificar al boton de lista usuarios no verificados el estado de la lista (abierta / cerrada)
      private noVerifySources = new Subject();
      noVerifyListEmitted = this.noVerifySources.asObservable();
      sendNoVerify(estado){
        this.noVerifySources.next(estado);
      }

      //Servicio para abrir y cerrar la lista de usuarios no verificados
      private openVerifyListSources = new Subject();
      openVerifyListEmitted = this.openVerifyListSources.asObservable();
      sendopenVerifyList(estado){
        this.openVerifyListSources.next(estado);
      }

      //Servicio para abrir anadidor de addUserComponent en la sección verificar cuenta
      private abrirAnadidSources = new Subject();
      abrirAnadidEmitted = this.abrirAnadidSources.asObservable();
      sendAbrirAnadid(estado){
        this.abrirAnadidSources.next(estado);
      }


      //Servicio para filtrar vistas de LISTAS DE USUARIO VIP- GENERAL -
      private listViewSources = new Subject();
      listViewEmitted = this.listViewSources.asObservable();
      sendlistView(estado){
        this.listViewSources.next(estado);
      }


      //Servicio para comunicar lista de usuarios con detalle usuario admin panel
      private userDetailSources = new Subject();
      userDetailEmitted = this.userDetailSources.asObservable();
      senduserDetail(usuario, vista, panel, editUser){
        const body = {
          usuario: usuario,
          vista: vista,
          panel: panel,
          editUser: editUser
        };

        this.userDetailSources.next(body);
      }



      //Servicio para Actualizar/Recargar lista de usuarios
      private listUpdateSources = new Subject();
      listUpdateEmitted = this.listUpdateSources.asObservable();
      sendListUpdate(estado){
        this.listUpdateSources.next(estado);
      }


      //Servicio para llamar popup de renovación membresía usuario
      private updateMembershipSources = new Subject();
      updateMembershipEmitted = this.updateMembershipSources.asObservable();
      sendUpdateMembership(status, payment){
        const body = {
          status:status,
          payment:payment
        }
        this.updateMembershipSources.next(body);
      }


      //Servicio para cargar mas proyectos en el feed Scroll Infinito manual
      private listProjectSources = new Subject();
      listProjectEmitted = this.listProjectSources.asObservable();
      sendlistProject(projects, visual){
        const body = {
          projects: projects,
          visual:visual
        }
        this.listProjectSources.next(body);
      }









      openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
          duration: 2000,
        });
      }




}
